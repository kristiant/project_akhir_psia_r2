<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PelunasanBeli extends Model
{
    protected $table = 'pelunasanbelis'; // nama tabel di mysql
    public $timestamps = false; // apakah di tabel ada timestamps


    public function notaBeliBarang()
    {
        return $this->belongsTo("App\NotaBeliBarang", 'notabelibarang_id');
    }
    public function notaBeliAset()
    {
        return $this->belongsTo("App\NotaBeliAset", 'notabeliaset_id');
    }
    public function bank()
    {
        return $this->belongsTo("App\Bank", 'bank_id');
    }
}
