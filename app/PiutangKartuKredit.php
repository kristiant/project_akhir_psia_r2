<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PiutangKartuKredit extends Model
{
    protected $table = 'piutangkartukredits'; // nama tabel di mysql
    public $timestamps = false; // apakah di tabel ada timestamps
    public $incrementing = true; // apakah primary key berupa autoincrement
    protected $primaryKey = 'id'; // apa nama column primary key di mysql
    protected $keyType = 'integer'; // apa tipe primary key nya
}
