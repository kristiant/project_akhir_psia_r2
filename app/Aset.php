<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aset extends Model
{
    public $timestamps = false; // apakah di tabel ada timestamps

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id');
    }

}
