<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    public $timestamps = false; // apakah di tabel ada timestamps

    public function beliAverage($argJumlahBeli, $argHargaSatuan, $argTambahanHarga = 0) // tambahan harga bisa dipakai untuk biaya kirim, dsb
    {
        // $argTambahanHarga untuk per 1 barang
        $totalHPP = ($this->harga_beli*$this->jumlah_stok_gudang) + ($argJumlahBeli * ($argHargaSatuan+$argTambahanHarga));
        $totalJumlahBarang = $argJumlahBeli+$this->jumlah_stok_gudang;

        $this->harga_beli = $totalHPP / $totalJumlahBarang;
        $this->jumlah_stok_gudang = $totalJumlahBarang;

        $this->save();
    }
}
