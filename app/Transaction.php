<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    public $timestamps = false; // apakah di tabel ada timestamps
    protected $fillable = ['registered_at', 'notes', 'reference_number', 'type'];

    public function accounts()
    {
        return $this->belongsToMany('App\Account', 'transaction_details', 'transaction_id', 'account_id')
            ->withPivot('taccount','value')
        ;
    }

    public function isBalanced()
    {
        $totalCredit = 0;
        $totalDebit =  0;

        foreach ($this->transactionDetails as $key => $detail)
        {
            if($detail->pivot->taccount == "CREDIT")
            {
                $totalCredit += $detail->pivot->value;
            }
            else if($detail->pivot->taccount == "DEBIT")
            {
                $totalDebit += $detail->pivot->value;
            }
        }

        return ($totalDebit == $totalCredit);
    }

    public function addTransactionDetail($argAccount, $argTAccountType, $argValue)
    {
        if($argTAccountType != "DEBIT" && $argTAccountType != "CREDIT")
        {
            throw new Exception("taccount must be (string) CREDIT or DEBIT !", 1);
        }
        if($this->accounts()->where('id','=',$argAccount->id)->count() != 0)
        {
            throw new Exception("account already exist in relation 'transaction_details'", 1);
        }

        $this->accounts()->attach($argAccount, ['taccount'=>$argTAccountType,'value'=>$argValue]);
    }

}
