<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaBeliAset extends Model
{
    protected $table = 'notabeliasets'; // nama tabel di mysql
    public $timestamps = false; // apakah di tabel ada timestamps

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }

    public function aset()
    {
        return $this->belongsTo('App\Aset', 'aset_id');
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank', 'bank_id');
    }

    public function notaPelunasan()
    {
        return $this->hasMany("App\PelunasanBeli", 'notabelibarang_id');
    }

    public function hitungSisaHutang($tanggal = "now")
    {
        if($this->is_lunas == true)
        {
            return ["sisa"=>0];
        }
        if($this->cara_bayar != "Cicilan")
        {
            return ["sisa"=>0];
        }


        $sisa = 0.0;

        // tambah sisa
        $sisa = $sisa + $this->harga_beli * $this->jumlah;
        if($this->fob == "FOB Shipping Point" && is_numeric($this->biaya_kirim) && $this->biaya_kirim > 0)
        {
            $sisa = $sisa + $this->biaya_kirim;
        }

        // kurangi dari uang muka yang sudah dibayarkan
        $sisa = $sisa - $this->uang_muka_cicilan;

        // kurangi dari yang sudah pernah dibayarkan
        foreach ($this->notaPelunasan as $key => $pelunasan)
        {
            $sisa = $sisa - $pelunasan->nominal;
        }
        return ["sisa"=>$sisa];
    }
}
