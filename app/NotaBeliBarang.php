<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaBeliBarang extends Model
{
    protected $table = 'notabelibarangs'; // nama tabel di mysql
    public $timestamps = false; // apakah di tabel ada timestamps

    public function detilBarang()
    {
        return $this->belongsToMany('App\Barang', 'notabelibarangs_barangs', 'notabelibarang_id', 'barang_id')
            ->withPivot('jumlah', 'harga')
            //->withTimestamps()
            //->wherePivot('is_valid', 1)
            ;
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank', 'bank_id');
    }

    public function notaPelunasan()
    {
        return $this->hasMany("App\PelunasanBeli", 'notabelibarang_id');
    }


    public function hitungTotalHargaBarang() // tidak termasuk harga kirim
    {
        $total = 0.0;
        foreach ($this->detilBarang as $key => $detil)
        {
            $total += ( $detil->pivot->jumlah * $detil->pivot->harga );
        }
        return $total;
    }

    public function hitungCountBarang()
    {
        $count = 0;
        foreach ($this->detilBarang as $key => $detil)
        {
            $count += ( $detil->pivot->jumlah );
        }
        return $count;
    }

    public function hitungSisaHutang($tanggal = "now")
    {
        if($this->is_lunas == true)
        {
            return ["sisa"=>0];
        }


        $sisa = 0.0;

        // tambah sisa
        $sisa = $sisa + $this->hitungTotalHargaBarang();
        if($this->fob == "FOB Shipping Point" && is_numeric($this->biaya_kirim) && $this->biaya_kirim > 0)
        {
            $sisa = $sisa + $this->biaya_kirim;
        }

        $diskon = null;
        // kurangi dari discount term (yang dipotong hanya dari total barangnya saja, tidak termasuk dari pengiriman)
        if($this->cara_bayar == "Kredit" && $this->discount_term != null) // $this->discount_term = "2/15 n/30"
        {
            $diskonString = explode(" ", $this->discount_term)[0]; // $diskonString = "2/15"
            $diskonPersen = explode("/", $diskonString)[0]; // $diskonPersen = 2
            $diskonHari = explode("/", $diskonString)[1]; // $diskonHari = 15

            $dateBeli = new \DateTime(date('Y-m-d', strtotime($this->tanggal)));
            $dateNow = new \DateTime(date('Y-m-d', strtotime($tanggal)));
            $bedaHari = $dateBeli->diff($dateNow)->days;

            if($bedaHari < $diskonHari)
            {
                $diskon = ($this->hitungTotalHargaBarang() * ($diskonPersen/100.0));
                $sisa = $sisa - $diskon;
            }
        }

        // kurangi dari yang sudah pernah dibayarkan
        foreach ($this->notaPelunasan as $key => $pelunasan)
        {
            $sisa = $sisa - $pelunasan->nominal;
        }

        if($diskon == null)
        {
            return ["sisa"=>$sisa];
        }
        else
        {
            return ["sisa"=>$sisa,"diskon"=>$diskon];
        }
    }

    public function potongHargaBeliBarangKarenaDiskon($argNominalDiskon, $argTanggalPotong) // return double (jika tidak 0 maka itu angka yang harus masuk di pendapatan lain-lain karena barangnya sudah habis atau HPP nya sudah nol)
    {
        $kelebihanYangMasukKePendapatanLainLain = 0;
        $countBarang = $this->hitungCountBarang();
        foreach ($this->detilBarang as $key => $detilBarang)
        {
            $hargaBeliBarangLama = $detilBarang->harga_beli;

            $rasio = $detilBarang->pivot->jumlah / $countBarang;
            // contoh rasio:
            // -> pembelian 10 barang A @Rp.6000 (sebelum pembelian di gudang sudah ada 230 item @Rp.5000),
            // -> dan 20 barang B @Rp.700 (sebelum pembelian di gudang sudah ada 4 item @Rp.1000),
            // -> dapat diskon Rp.3000,
            // maka:
            // rasio untuk barang A = 10/30 = 1/3
            // potong harga stok secara keseluruhan untuk barang A sejumlah 1/3 x Rp.3000 = Rp.1000
            // total nilai stok di gudang A sebelum dipotong: totalNilaiStokA = (10x6000) + (230x5000) = 1 210 000
            // dipotong 1000 jadi = 1 210 000 - 1000 = 1 209 000
            // dikembalikan ke db dg cara: 1 209 000/(10+230) = 5037.5
            // harga awal per barang sebelum dipotong diskon = 1 210 000/(10+230) = 5041.667
            $nominalPotongSecaraKeseluruhan = $rasio * $argNominalDiskon;

            if($detilBarang->jumlah_stok_gudang == 0)
            {
                $kelebihanYangMasukKePendapatanLainLain = $kelebihanYangMasukKePendapatanLainLain + $nominalPotongSecaraKeseluruhan;
            }
            else
            {
                $totalNilaiStok = $detilBarang->harga_beli * $detilBarang->jumlah_stok_gudang;

                $totalNilaiStok = $totalNilaiStok - $nominalPotongSecaraKeseluruhan;
                if($totalNilaiStok < 0)
                {
                    $kelebihanYangMasukKePendapatanLainLain = $kelebihanYangMasukKePendapatanLainLain + abs($totalNilaiStok);
                    $totalNilaiStok = 0;
                }

                $detilBarang->harga_beli = ($totalNilaiStok / $detilBarang->jumlah_stok_gudang);
                $detilBarang->save();
            }



            $hargaBeliBarangBaru = $detilBarang->harga_beli;
            if($hargaBeliBarangLama != $hargaBeliBarangBaru)
            {
                $historyBarang = new BarangHistory();
                $historyBarang->barang_id = $detilBarang->id;
                $historyBarang->tanggal = $argTanggalPotong; //$this->tanggal;
                $historyBarang->jumlah = null;
                $historyBarang->harga = null;
                $historyBarang->saldo_jumlah = $detilBarang->jumlah_stok_gudang;
                $historyBarang->saldo_harga = $detilBarang->harga_beli;
                $historyBarang->save();
            }

        }
        return $kelebihanYangMasukKePendapatanLainLain;
    }

}
