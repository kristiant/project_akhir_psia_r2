<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SewaProperti extends Model
{
    protected $table = 'sewapropertis'; // nama tabel di mysql
    public $timestamps = false; // apakah di tabel ada timestamps

    public function bank()
    {
        return $this->belongsTo('App\Bank', 'bank_id');
    }
}
