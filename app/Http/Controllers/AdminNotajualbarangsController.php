<?php
namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;

class AdminNotajualbarangsController extends \crocodicstudio\crudbooster\controllers\CBController {

    public function cbInit() {

		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "id";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = true;
		$this->button_export = true;
		$this->table = "notajualbarangs";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Tanggal","name"=>"tanggal"];
		$this->col[] = ["label"=>"Status","name"=>"status"];
		$this->col[] = ["label"=>"Fob","name"=>"fob"];
		$this->col[] = ["label"=>"Biaya Kirim","name"=>"biaya_kirim"];
		// $this->col[] = ["label"=>"Resi Kirim","name"=>"resi_kirim"];
		$this->col[] = ["label"=>"Cara Bayar","name"=>"cara_bayar"];
		$this->col[] = ["label"=>"Nomor Kartu Kredit","name"=>"nomor_kartu_kredit"];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'datetime','validation'=>'required|date_format:Y-m-d H:i:s','width'=>'col-sm-10'];
		// $this->form[] = ['label'=>'Status','name'=>'status','type'=>'select','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'MASUK;TERBAYAR;PACKING;DIKIRIM;SELESAI'];
		$this->form[] = ['label'=>'Fob','name'=>'fob','type'=>'select','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'FOB Shipping Point;FOB Destination'];
		$this->form[] = ['label'=>'Biaya Kirim (Rupiah)','name'=>'biaya_kirim','type'=>'money','validation'=>'integer|min:0','width'=>'col-sm-10','placeholder'=>'(dalam Rupiah)'];
		// $this->form[] = ['label'=>'Resi Kirim','name'=>'resi_kirim','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Cara Bayar','name'=>'cara_bayar','type'=>'select','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'Tunai Cash;Tunai Transfer;Kartu Kredit'];
        $this->form[] = ['label'=>'Bank Id Perusahaan','name'=>'bank_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'banks,nama'];
        $this->form[] = ['label'=>'Nomor Rekening Customer','name'=>'nomor_rekening','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','placeholder'=>'Nomor Rekening Customer'];
		$this->form[] = ['label'=>'Nomor Kartu Kredit','name'=>'nomor_kartu_kredit','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Discount Term','name'=>'discount_term','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','placeholder'=>'contoh: "2/15 n/30"'];

        // field-field milik pivot
        $columns[] = ['label'=>'Barang','name'=>'barang_id','type'=>'datamodal','datamodal_table'=>'barangs','datamodal_columns'=>'nama','datamodal_select_to'=>'nama:nama','datamodal_where'=>'','datamodal_size'=>'large'];
        $columns[] = ['label'=>'Jumlah','name'=>'jumlah','type'=>'number','required'=>true];
        $columns[] = ['label'=>'Harga (Rupiah/Satuan)','name'=>'harga','type'=>'number','required'=>true];
        $columns[] = ['label'=>'Diskon (Rupiah/Satuan)','name'=>'diskon','type'=>'number','required'=>true];
        $this->form[] = ['label'=>'Detail Barang','name'=>'notajualbarangs_barangs','type'=>'child','columns'=>$columns,'table'=>'notajualbarangs_barangs','foreign_key'=>'notajualbarang_id'];
		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'datetime','validation'=>'required|date_format:Y-m-d H:i:s','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Status','name'=>'status','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Fob','name'=>'fob','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Biaya Kirim','name'=>'biaya_kirim','type'=>'money','validation'=>'integer|min:0','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Resi Kirim','name'=>'resi_kirim','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Cara Bayar','name'=>'cara_bayar','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Nomor Kartu Kredit','name'=>'nomor_kartu_kredit','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Bank Id','name'=>'bank_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'bank,id'];
		//$this->form[] = ['label'=>'Discount Term','name'=>'discount_term','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
		# OLD END FORM

		/*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert        = array();



        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();



        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();



        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;



        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;



        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js[] = asset('js/crudbooster/extension_crudbooster_notajualbarang.js');



        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;



        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected,$button_name) {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index,&$column_value) {
    	//Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id) {
        $nota = \App\NotaJualBarang::find($id);


        //TAMBAHKAN KE PIUTANG KARTU KREDIT JIKA BAYARNYA PAKAI KARTU KREDIT
        if($nota->cara_bayar == "Kartu Kredit")
        {
        	$piutangCC = new \App\PiutangKartuKredit();

	        $piutangCC->tanggal = $nota->tanggal;
	        $piutangCC->keterangan = "JUAL-" . $id;
	        $piutangCC->bank_id = $nota->bank_id;
	        $piutangCC->nomor_kartu_kredit = $nota->nomor_kartu_kredit;
	        $piutangCC->nominal = $nota->hitungTotalPenjualanKotor();
	        $piutangCC->is_sudah_ditarik = false;
	        $piutangCC->save();
        }

        //JURNALKAN
        $transaction = new \App\Transaction();
        $transaction->registered_at = $nota->tanggal;
        $transaction->notes = "JUAL-" . $id;
        $transaction->type = "NORMAL";
        $transaction->save();

        // ambil account-account yang diperlukan
        $accountSediaanStok = \App\Account::find(107); // sediaan stok
        $accountHPP = \App\Account::find(501); // HPP
        $accountIncomePenjualan = \App\Account::find(401); // Penjualan
        $accountIncomeReverseDiskonPenjualan = \App\Account::find(402); // Diskon Penjualan

        $accountCaraBayar = null;
        if($nota->cara_bayar == "Tunai Cash")
        {
            $accountCaraBayar = \App\Account::find(101); // Kas di tangan
        }
        else if($nota->cara_bayar == "Tunai Transfer")
        {
            if($nota->bank->nama == "ABC")
            {
                $accountCaraBayar = \App\Account::find(102); // Rekening bank ABC
            }
            else if($nota->bank->nama == "XYZ")
            {
                $accountCaraBayar = \App\Account::find(103); // Rekening bank XYZ
            }
        }
        else if($nota->cara_bayar == "Cicilan")
        {
            $accountCaraBayar = \App\Account::find(104); // Piutang dagang
        }
        else if($nota->cara_bayar == "Kartu Kredit")
        {
            $accountCaraBayar = \App\Account::find(105); // Piutang kartu kredit
        }

        // jurnalkan
        $biayaKirim = 0;
        if($nota->fob == "FOB Destination" && is_numeric($nota->biaya_kirim) && $nota->biaya_kirim != null && $nota->biaya_kirim > 0)
        {
            $biayaKirim = $nota->biaya_kirim;
            $transaction->accounts()->attach($accountBiayaKirim, ['taccount'=>'DEBIT','value'=>$biayaKirim]);
        }

        $transaction->accounts()->attach($accountCaraBayar, ['taccount'=>'DEBIT',
            'value'=> ( $nota->hitungTotalPenjualanKotor() - $nota->hitungTotalDiskon() - $biayaKirim )
        ]); // dapat pembayaran (misal +cash / +rekening / +piutang)
        if($nota->hitungTotalDiskon() != 0) // jika ada diskon
        {
            $transaction->accounts()->attach($accountIncomeReverseDiskonPenjualan, ['taccount'=>'DEBIT',
                'value'=> ( $nota->hitungTotalDiskon() )
            ]);
        }
        $transaction->accounts()->attach($accountIncomePenjualan, ['taccount'=>'CREDIT',
            'value'=> ( $nota->hitungTotalPenjualanKotor() )
        ]); // income.penjualan

        // hpp
        $transaction->accounts()->attach($accountHPP, ['taccount'=>'DEBIT','value'=>$nota->hitungTotalHPP()]);
        $transaction->accounts()->attach($accountSediaanStok, ['taccount'=>'CREDIT','value'=>$nota->hitungTotalHPP()]);

        // potong jumlah stok
        foreach ($nota->detilBarang as $key => $detilBarang)
        {
            $detilBarang->jumlah_stok_gudang = $detilBarang->jumlah_stok_gudang - $detilBarang->pivot->jumlah;
            $detilBarang->save();

            // kartu stok
            $barangHistory = new \App\BarangHistory();
            $barangHistory->barang_id = $detilBarang->id;
            $barangHistory->tanggal = $nota->tanggal;
            $barangHistory->jumlah = -1*($detilBarang->pivot->jumlah);
            $barangHistory->harga = $detilBarang->harga_beli;
            $barangHistory->saldo_jumlah = $detilBarang->jumlah_stok_gudang;
            $barangHistory->saldo_harga = $detilBarang->harga_beli;
            $barangHistory->save();
        }

        

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata,$id) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id) {
        //Your code here

    }



    //By the way, you can still create your own method in here... :)


}