<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Period;
use App\Account;
use App\Product;
use App\Transaction;
use App\TransactionDetail;

class SamplePagesController extends Controller
{
    public function journal($period_id)
    {
        $period = Period::find($period_id);
        return view('sample.journal', ['period' => $period]);
    }
    public function ledger($period_id)
    {
        $period = Period::find($period_id);
        $accounts = Account::all();
        return view('sample.ledger', ['period' => $period, 'accounts' => $accounts]);
    }
    public function report($period_id)
    {
        $period = Period::find($period_id);
        $report = $period->generateReports();
        return view('sample.report', ['period' => $period, 'report' => $report]);
    }
    public function worksheet($period_id)
    {
        $period = Period::find($period_id);
        $accounts = Account::all();
        return view('sample.worksheet', ['period' => $period, 'accounts' => $accounts]);
    }
    public function stock_card($product_id)
    {
        $product = Product::find($product_id);
        return view('sample.stock_card', ['product' => $product]);
    }


    public function input_transaction()
    {
        return view('sample.input_transaction');
    }
    public function input_transaction_helper(Request $request)
    {
        return redirect(route('transactionmanual.input.details', ['number_of_details' => $request->number_of_transaction]));
    }
    public function input_transaction_details($number_of_details)
    {
        $accounts = Account::all();
        return view('sample.input_transaction_details', ['number_of_details' => $number_of_details, 'accounts' => $accounts]);
    }
    public function store_transaction(Request $request)
    {
        //dd($request->detail_account_id);

        $tr = new Transaction();
        $tr->registered_at = $request->transaction_registered_at;
        $tr->notes = $request->transaction_notes;
        $tr->type = $request->transaction_type;
        $tr->save();

        $trdtls = [];
        for ($i = 0; $i < $request->number_of_details; $i++)
        {
            $trdtl = new TransactionDetail();
            $trdtl->transaction_id = $tr->id;
            $trdtl->taccount = $request->detail_taccount[$i];
            $trdtl->account_id = $request->detail_account_id[$i];
            $trdtl->value = $request->detail_value[$i];
            $trdtl->save();
            $trdtls[] = $trdtl;
        }

        //dd($trdtls);
        //return redirect(route('sample_journal', ['period_id' => 1]));

        // cek apakah sudah balance
        if($tr->isBalanced())
        {
            return "sukses!";
        }
        else
        {
            return "tidak balance!";
        }
    }








    public function input_transaction_sell_lifo()
    {
        return view('sample.input_transaction_sell_lifo');
    }
    public function input_transaction_helper_sell_lifo(Request $request)
    {
        return redirect(route('sell.lifo.details',
            [
                'number_of_products' => $request->number_of_products,
                'number_of_accounts' => $request->number_of_accounts
            ]
        ));

        //return $this->input_transaction_details_sell_lifo($request->number_of_products, $request->number_of_accounts);
    }
    public function input_transaction_details_sell_lifo($number_of_products, $number_of_accounts)
    {
        $accounts = Account::all();
        $products = Product::all();
        return view('sample.input_transaction_details_sell_lifo',
            [
                'number_of_products' => $number_of_products,
                'number_of_accounts' => $number_of_accounts,
                'accounts' => $accounts,
                'products' => $products
            ]
        );
    }
    public function store_transaction_sell_lifo(Request $request)
    {
        //return "null";


        $tr = new Transaction();
        $tr->registered_at = $request->transaction_registered_at;
        $tr->notes = $request->transaction_notes;
        $tr->type = "NORMAL";
        $tr->reference_number = $request->transaction_reference;
        $tr->save();

        $totalHpp = 0.0;
        $trdtls = [];

        // hitung HPP
        for ($i = 0; $i < $request->number_of_products; $i++)
        {
            $product = Product::find($request->detail_product_id[$i]);
            $qty = $request->detail_product_qty[$i];
            $totalHpp += $product->lifoPop($qty, $tr->registered_at);
        }
        $trdtl = new TransactionDetail();
        $trdtl->transaction_id = $tr->id;
        $trdtl->taccount = "DEBIT";
        $trdtl->account_id = 501; // ini kode account HPP
        $trdtl->value = $totalHpp;
        $trdtl->save();
        $trdtls[] = $trdtl;

        // masukkan hpp ke pengurangan ASSET.STOK_BARANG
        $trdtl = new TransactionDetail();
        $trdtl->transaction_id = $tr->id;
        $trdtl->taccount = "CREDIT";
        $trdtl->account_id = 107; // ini kode account asset stok barang
        $trdtl->value = $totalHpp;
        $trdtl->save();
        $trdtls[] = $trdtl;

        // jika ada diskon
        if( isset($request->transaction_is_discount) && $request->transaction_is_discount == true )
        {
            $trdtl = new TransactionDetail();
            $trdtl->transaction_id = $tr->id;
            $trdtl->taccount = "DEBIT";
            $trdtl->account_id = 402; // ini kode account diskon
            $trdtl->value = $request->transaction_discount_value;
            $trdtl->save();
            $trdtls[] = $trdtl;
        }

        /* // di comment karena aku gak tahu biaya pengiriman masuk account mana di kasus ini
        // jika ada biaya pengiriman
        if( isset($request->transaction_is_delivery) && $request->transaction_is_delivery == true )
        {
            $trdtl = new TransactionDetail();
            $trdtl->transaction_id = $tr->id;
            $trdtl->taccount = "DEBIT";
            $trdtl->account_id = 520; // ini kode account biaya lain-lain
            $trdtl->value = $request->transaction_delivery_value;
            $trdtl->save();
            $trdtls[] = $trdtl;
        }
        */


        // input metode pembayaran
        for ($i = 0; $i < $request->number_of_accounts; $i++)
        {
            $trdtl = new TransactionDetail();
            $trdtl->transaction_id = $tr->id;
            $trdtl->taccount = "CREDIT";
            $trdtl->account_id = $request->detail_account_id[$i];
            $trdtl->value = $request->detail_account_value[$i];
            $trdtl->save();
            $trdtls[] = $trdtl;
        }

        // masukkan sisa ke pendapatan penjualan (atau masukkan ke rugi penjualan jika minus)
        $untung = 0.0;
        $totalDebit = 0.0;
        $totalCredit = 0.0;
        foreach ($trdtls as $key => $trdtl)
        {
            if($trdtl->taccount == "DEBIT")
            {
                $totalDebit += $trdtl->value;
            }
            else
            {
                $totalCredit += $trdtl->value;
            }
        }
        $untung = $totalDebit - $totalCredit;
        $trdtl = new TransactionDetail();
        $trdtl->transaction_id = $tr->id;
        if($untung > 0)
        {
            $trdtl->taccount = "CREDIT";
            $trdtl->account_id = 401; // ini kode account pendapatan penjualan
            $trdtl->value = $untung;

        }
        else
        {
            $trdtl->taccount = "DEBIT";
            $trdtl->account_id = 520; // ini kode account biaya/rugi lain-lain
            $trdtl->value = abs($untung);
        }
        $trdtl->save();
        $trdtls[] = $trdtl;

        // cek apakah sudah balance
        if($tr->isBalanced())
        {
            return "sukses!";
        }
        else
        {
            return "tidak balance!";
        }
    }



    public function testing()
    {
        $period = Period::find(1);
        //$accountKas = Account::find(101);
        //dd($period->getTransactions());
        //dd($period->getOpeningBalance($accountKas));

        $period->closePeriod('2018-07-31 20:00:00');
    }

}
