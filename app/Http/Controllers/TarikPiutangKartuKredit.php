<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TarikPiutangKartuKredit extends Controller
{
    public function showFormPilihBank()
    {
        $args = [];
        $banks = \App\Bank::all();

        $args['banks'] = $banks;
        return view('tarikpiutangkartukredit.formpilihbank', $args);
    }
    public function pilihBank(Request $request)
    {
        if(isset($request->bank_id))
        {
            return redirect(route('TarikPiutangKartuKredit.showFormTarik', ['bank_id' => $request->bank_id]));
        }
    }
    public function showFormTarik($bank_id)
    {
        $args = [];
        $bank = \App\Bank::find($bank_id);
        $arrPiutang = \App\PiutangKartuKredit::where('bank_id','=',$bank->id)->where('is_sudah_ditarik','=',false)->get();


        $args['bank'] = $bank;
        $args['arrPiutang'] = $arrPiutang;
        return view('tarikpiutangkartukredit.formtarik', $args);
    }
    public function proses(Request $request)
    {
        // terima args: $bank_id, $arr_piutangkartukredit_id[], $tanggal_tarik, // GK JADI : $account_terima

        $totalTarik = 0.0;
        foreach ($request->arr_piutangkartukredit_id as $key => $item)
        {
            $piutang = \App\PiutangKartuKredit::find($item);
            $piutang->is_sudah_ditarik = true;
            $piutang->ditarik_tanggal = $request->tanggal_tarik; //date('Y-m-d H:i:s');
            $piutang->save();
            $totalTarik = $totalTarik + $piutang->nominal;
        }

        // jurnalkan
        $transaction = new \App\Transaction();
        $transaction->registered_at = $request->tanggal_tarik;
        $transaction->notes = "TPKK-" . implode(",", $request->arr_piutangkartukredit_id);
        $transaction->type = "NORMAL";
        $transaction->save();

        //$accountTerima = Account::find($request->account_terima);
        $accountTerima = null;
        $bank = \App\Bank::find($request->bank_id);
        if($bank->nama == "ABC")
        {
            $accountTerima = \App\Account::find(102); // rekening bank ABC
        }
        else if($bank->nama == "XYZ")
        {
            $accountTerima = \App\Account::find(103); // rekening bank XYZ
        }
        else
        {
            $accountTerima = \App\Account::find(101); // kas di tangan
        }
        $accountPiutangKartuKredit = \App\Account::find(105);
        $transaction->accounts()->attach($accountTerima, ['taccount'=>'DEBIT','value'=>$totalTarik]);
        $transaction->accounts()->attach($accountPiutangKartuKredit, ['taccount'=>'CREDIT','value'=>$totalTarik]);

        $args = [];
        $banks = \App\Bank::all();
        $args['banks'] = $banks;
        return view('tarikpiutangkartukredit.formpilihbank', $args);

    }
}
