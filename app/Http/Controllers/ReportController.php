<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Period;
use App\Account;
use App\Barang;
use App\Transaction;
use App\TransactionDetail;
use App\BarangHistory;

class ReportController extends Controller
{
	public function showIndex()
	{
		$args = [];
		$periods = Period::all();
		$products = Barang::all();

		$args['periods'] = $periods;
		$args['products'] = $products;
		return view('report.index', $args);
	}
	public function chooseReport(Request $request)
	{
		if(isset($request->period_id))
		{
			$type = $request->report_type;
			switch ($type) {
				case 'journal':
					return redirect(route('Report.showJournal', ['period_id' => $request->period_id]));
					break;

				case 'ledger':
					return redirect(route('Report.showLedger', ['period_id' => $request->period_id]));
					break;

				case 'worksheet':
					return redirect(route('Report.showWorksheet', ['period_id' => $request->period_id]));
					break;

				case 'reports':
					return redirect(route('Report.showReports', ['period_id' => $request->period_id]));
					break;

				case 'stock':
					return redirect(route('Report.showStock', ['product_id' => $request->product_id]));
					break;
				
				default:
					return redirect(route('Report.showIndex'));
					break;
			}
		}
	}
    public function showJournal($period_id)
    {
        $period = Period::find($period_id);
        return view('report.journal', ['period' => $period]);
    }
    public function showLedger($period_id)
    {
        $period = Period::find($period_id);
        $accounts = Account::all();
        return view('report.ledger', ['period' => $period, 'accounts' => $accounts]);
    }
    public function showReports($period_id)
    {
        $period = Period::find($period_id);
        $report = $period->generateReports();
        return view('report.reports', ['period' => $period, 'report' => $report]);
    }
    public function showWorksheet($period_id)
    {
        $period = Period::find($period_id);
        $accounts = Account::all();
        return view('report.worksheet', ['period' => $period, 'accounts' => $accounts]);
    }
    public function showStock($product_id)
    {
    	$product = Barang::find($product_id);
        $histories = BarangHistory::where('barang_id', '=', $product_id)->get();
        return view('report.stock', ['product' => $product, 'histories' => $histories]);
    }
}
