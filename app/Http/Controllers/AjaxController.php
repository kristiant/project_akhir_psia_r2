<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function sisaHutangBarang($nota_beli_barang_id = null, $tanggal = "now")
    {
        if($nota_beli_barang_id == "" || $nota_beli_barang_id == 0 || $nota_beli_barang_id == null)
        {
            return json_encode(["status"=>"failed"]);
        }
        else
        {
            $nota = \App\NotaBeliBarang::find($nota_beli_barang_id);
            $arrResult = $nota->hitungSisaHutang($tanggal);
            $arrResult["status"] = "success";
            return json_encode($arrResult);
        }
    }

    public function sisaHutangAset($nota_beli_aset_id = null, $tanggal = "now")
    {
        if($nota_beli_aset_id == "" || $nota_beli_aset_id == 0 || $nota_beli_aset_id == null)
        {
            return json_encode(["status"=>"failed"]);
        }
        else
        {
            $nota = \App\NotaBeliAset::find($nota_beli_aset_id);
            $arrResult = $nota->hitungSisaHutang($tanggal);
            $arrResult["status"] = "success";
            return json_encode($arrResult);
        }
    }
}
