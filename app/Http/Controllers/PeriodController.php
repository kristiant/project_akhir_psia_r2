<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Period;

class PeriodController extends Controller
{
    public function showPeriod()
    {
    	$args = [];
    	$periods = Period::all();

		$args['periods'] = $periods;
    	return view('period.periods', $args);
    }

    public function closePeriod(Request $request)
    {
    	$period_id = $request->period_id;
    	$close_date = $request->close_date;

    	$period = Period::find($period_id);
        $period->closePeriod($close_date);

        return redirect(route('ClosePeriod.showPeriod'));
    }
}
