<?php
namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
//use App\PelunasanBeli;

class AdminPelunasanbeliController extends \crocodicstudio\crudbooster\controllers\CBController {

    public function cbInit() {

		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "id";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = true;
		$this->button_export = true;
		$this->table = "pelunasanbelis";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
        $this->col[] = ["label"=>"Tanggal","name"=>"tanggal"];
		$this->col[] = ["label"=>"Notabelibarang Id","name"=>"notabelibarang_id","join"=>"notabelibarangs,tanggal"];
		$this->col[] = ["label"=>"Notabeliaset Id","name"=>"notabeliaset_id","join"=>"notabeliasets,tanggal"];
		$this->col[] = ["label"=>"Nominal","name"=>"nominal"];
        $this->col[] = ["label"=>"Cara Bayar","name"=>"cara_bayar"];
		$this->col[] = ["label"=>"Biaya Transfer","name"=>"biaya_transfer"];
		$this->col[] = ["label"=>"Nomor Rekening","name"=>"nomor_rekening"];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
        $this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'datetime','validation'=>'required|date_format:Y-m-d H:i:s','width'=>'col-sm-10'];
        // $this->form[] = ['label'=>'Jenis Nota','name'=>'jenis_nota','type'=>'select','validation'=>'min:1|max:255','width'=>'col-sm-10','dataenum'=>'Note Beli Barang;Nota Beli Aset'];
		$this->form[] = ['label'=>'Notabelibarang','name'=>'notabelibarang_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'notabelibarangs,tanggal','datatable_where'=>'cara_bayar = \'Kredit\''];
		$this->form[] = ['label'=>'Notabeliaset','name'=>'notabeliaset_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'notabeliasets,tanggal','datatable_where'=>'cara_bayar = \'Cicilan\''];
		$this->form[] = ['label'=>'Nominal (Rupiah)','name'=>'nominal','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10','placeholder'=>'(dalam Rupiah)'];
		$this->form[] = ['label'=>'Cara Bayar','name'=>'cara_bayar','type'=>'select','validation'=>'min:1|max:255','width'=>'col-sm-10','dataenum'=>'Tunai Cash;Tunai Transfer'];
        $this->form[] = ['label'=>'Bank Id Perusahaan','name'=>'bank_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'banks,nama'];
		$this->form[] = ['label'=>'Nomor Rekening Tujuan','name'=>'nomor_rekening','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','placeholder'=>'Nomor Rekening Tujuan'];
        $this->form[] = ['label'=>'Biaya Transfer (Rupiah)','name'=>'biaya_transfer','type'=>'money','validation'=>'integer|min:0','width'=>'col-sm-10','placeholder'=>'(dalam Rupiah)'];
		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ['label'=>'Notabelibarang Id','name'=>'notabelibarang_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'notabelibarangs,tanggal'];
		//$this->form[] = ['label'=>'Notabeliaset Id','name'=>'notabeliaset_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'notabeliasets,tanggal'];
		//$this->form[] = ['label'=>'Nominal','name'=>'nominal','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Biaya Transfer','name'=>'biaya_transfer','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Nomor Rekening','name'=>'nomor_rekening','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		# OLD END FORM

		/*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert        = array();



        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();



        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();



        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;



        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;



        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();
        $this->load_js[] = asset("js/crudbooster/extension_crudbooster_pelunasanbeli.js");



        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;



        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected,$button_name) {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index,&$column_value) {
    	//Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id) {
        $pelunasan = \App\PelunasanBeli::find($id);
        //$pelunasan = PelunasanBeli::find($id);

        // decide akun cara bayar
        $accountCaraBayar = null;
        if($pelunasan->cara_bayar == "Tunai Cash")
        {
            $accountCaraBayar = \App\Account::find(101); // kas
        }
        else if($pelunasan->cara_bayar == "Tunai Transfer")
        {
            if($pelunasan->bank->nama == "ABC")
            {
                $accountCaraBayar = \App\Account::find(102); // Rekening bank ABC
            }
            else if($pelunasan->bank->nama == "XYZ")
            {
                $accountCaraBayar = \App\Account::find(103); // Rekening bank XYZ
            }
        }
        else
        {
            throw new Exception("Unknown cara bayar", 1);
        }
        //echo "bank"; dd($pelunasan->bank); exit();

        // decide account hutang yang dibayar
        $notesText = "";    $accountHutangYangAkanDibayar = null;    $notaBeliYangAkanDibayar = null;
        if($pelunasan->notaBeliBarang == null && $pelunasan->notaBeliAset != null) //aset
        {
            $accountHutangYangAkanDibayar = \App\Account::find(202); // hutang bank
            $notesText = "LASE-" . $id;
            $notaBeliYangAkanDibayar = $pelunasan->notaBeliAset;
        }
        else if($pelunasan->notaBeliAset == null && $pelunasan->notaBeliBarang != null) //barang
        {
            $accountHutangYangAkanDibayar = \App\Account::find(201); // hutang dagang
            $notesText = "LBRG-" . $id;
            $notaBeliYangAkanDibayar = $pelunasan->notaBeliBarang;
        }
        else
        {
            throw new Exception("PelunasanBeli ini tidak diketahui mau melunasi pembelian aset atau barang!!", 1);
        }
        $sisaHutangArr = $notaBeliYangAkanDibayar->hitungSisaHutang($pelunasan->tanggal);
        $sisaHutangArr["sisa"] += $pelunasan->nominal; // added, karena supaya tidak hilang yg sekarang baru saja mau dibayar
        //dd($sisaHutangArr); dd($notaBeliYangAkanDibayar); exit();

        // jurnalkan
        $transaction = new \App\Transaction();
        $transaction->registered_at = $pelunasan->tanggal;
        $transaction->notes = $notesText;
        $transaction->type = "NORMAL";
        $transaction->save();

        // jika ada biaya transfer
        $biayaTransfer = 0.0;
        if($pelunasan->cara_bayar == "Tunai Transfer" && is_numeric($pelunasan->biaya_transfer) && $pelunasan->biaya_transfer > 0)
        {
            $accountBiayaLainLain = \App\Account::find(520); // Biaya/Rugi lain-lain
            $transaction->accounts()->attach($accountBiayaLainLain, ['taccount'=>'DEBIT','value'=>$pelunasan->biaya_transfer]);
            $biayaTransfer = $pelunasan->biaya_transfer;
        }

        $diskonNominal = 0.0;
        if(isset($sisaHutangArr["diskon"]) && is_numeric($sisaHutangArr["diskon"]) && $sisaHutangArr["diskon"] > 0 // ada diskon
            && $sisaHutangArr["sisa"] == $pelunasan->nominal) // dilunasi sekarang
        {

            $diskonNominal = $sisaHutangArr["diskon"];

            $accountTargetDiskon = null;
            $kelebihanDiskonYangMasukPendapatanLainLain = 0;
            if($pelunasan->notaBeliAset == null && $pelunasan->notaBeliBarang != null) // barang, dan ada diskon
            {
                $accountTargetDiskon = \App\Account::find(107); // Sediaan stok
                $kelebihanDiskonYangMasukPendapatanLainLain = $pelunasan->notaBeliBarang->potongHargaBeliBarangKarenaDiskon($diskonNominal, $pelunasan->tanggal);
                if($kelebihanDiskonYangMasukPendapatanLainLain > 0)
                {
                    $accountPendapatanLainLain = \App\Account::find(405);
                    $transaction->accounts()->attach($accountPendapatanLainLain, [
                        'taccount'=>'CREDIT',
                        'value'=>$kelebihanDiskonYangMasukPendapatanLainLain
                    ]);
                }
            }
            else if($pelunasan->notaBeliBarang == null && $pelunasan->notaBeliAset != null) // aset, dan ada diskon
            {
                $accountTargetDiskon = \App\Account::find(405); // Pendapatan lain-lain
            }
            $transaction->accounts()->attach($accountTargetDiskon, [
                'taccount'=>'CREDIT',
                'value'=> ($diskonNominal - $kelebihanDiskonYangMasukPendapatanLainLain)
            ]);
        }

        $transaction->accounts()->attach($accountHutangYangAkanDibayar, ['taccount'=>'DEBIT','value'=>(
            $pelunasan->nominal + $diskonNominal
        )]);
        $transaction->accounts()->attach($accountCaraBayar, ['taccount'=>'CREDIT','value'=>(
            $pelunasan->nominal + $biayaTransfer
        )]);

        // cek jika sudah lunas
        if($sisaHutangArr["sisa"] == $pelunasan->nominal)
        {
            $notaBeliYangAkanDibayar->is_lunas = true;
            $notaBeliYangAkanDibayar->save();
        }
        //exit();



    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata,$id) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id) {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id) {
        //Your code here

    }



    //By the way, you can still create your own method in here... :)


}