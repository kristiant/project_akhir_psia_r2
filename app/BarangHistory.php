<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangHistory extends Model
{
    protected $table = 'barangs_history'; //
    public $timestamps = false; // apakah di tabel ada timestamps

    public function barang()
    {
        return $this->belongsTo('App\Barang', 'barang_id');
    }

    public function ambilSaldoJumlahTerakhir($idBarang)
    {
    	$saldo = DB::select("
    		SELECT saldo_jumlah
    		FROM barangs_history
    		WHERE barang_id = $idBarang
    		ORDER BY tanggal DESC LIMIT 1");

    	return $saldo;
    }
}
