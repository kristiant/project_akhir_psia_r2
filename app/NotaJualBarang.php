<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaJualBarang extends Model
{
    protected $table = 'notajualbarangs'; // nama tabel di mysql
    public $timestamps = false; // apakah di tabel ada timestamps

    public function detilBarang()
    {
        return $this->belongsToMany('App\Barang', 'notajualbarangs_barangs', 'notajualbarang_id', 'barang_id')
            ->withPivot('jumlah', 'harga', 'diskon')
            //->withTimestamps()
            //->wherePivot('is_valid', 1)
            ;
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank', 'bank_id');
    }


    public function hitungTotalPenjualanKotor() // masuk account 'INCOME.penjualan'
    {
        $total = 0.0;

        foreach ($this->detilBarang as $key => $detil)
        {
            $jumlah = $detil->pivot->jumlah;
            $harga_satuan = $detil->pivot->harga;
            $diskon_satuan = $detil->pivot->diskon;

            //$total += ( ($harga_satuan - $diskon_satuan) * $jumlah );
            $total += ( $harga_satuan * $jumlah );
        }

        /*
        if($this->fob == "FOB Destination")
        {
            $total += $this->biaya_kirim;
        }
        */

        return $total;
    }

    public function hitungTotalDiskon() // masuk account 'INCOME-1.diskon'
    {
        $total = 0.0;

        foreach ($this->detilBarang as $key => $detil)
        {
            $jumlah = $detil->pivot->jumlah;
            $harga_satuan = $detil->pivot->harga;
            $diskon_satuan = $detil->pivot->diskon;

            $total += ( $diskon_satuan * $jumlah );
        }
        return $total;
    }

    public function hitungTotalHPP() // masuk account 'EXPENSE.hpp'
    {
        $hpp = 0.0;

        foreach ($this->detilBarang as $key => $detil)
        {
            $harga_beli_barang = $detil->harga_beli;
            $jumlah = $detil->pivot->jumlah;

            $hpp += $harga_beli_barang * $jumlah;
        }

        return $hpp;
    }

    // untuk account 'ASSET.cash' atau 'ASSET.rekeningbank', ambil dari hitungTotalPenjualanKotor()-hitungTotalDiskon()
}
