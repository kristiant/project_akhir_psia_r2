<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Account extends Model
{
    protected $table = 'accounts';
    public $incrementing = false;

    /*
    //public function transactionDetails()
    //{
    //    return $this->belongsToMany('App\Transaction', 'transaction_details', 'account_id', 'transaction_id')
    //        ->withPivot('taccount','value')
    //    ;
    //}
    */

    public function getAccumulationOfAllTransactionInPeriod($period, $argTransactionTypeArray = null)
    {
        if($argTransactionTypeArray == null)
        {
            $argTransactionTypeArray = ["'NORMAL'", "'CORRECTION'", "'ADJUSTMENT'"];
        }
        $typeIn = implode(", ", $argTransactionTypeArray);

        $accountResult = DB::select("
        SELECT
            SUM(IF(td.taccount = a.normal_balance_position, td.value, -1 * td.value)) AS summary_value
        FROM
            accounts a
            INNER JOIN transaction_details td ON td.account_id=a.id
            INNER JOIN transactions t ON td.transaction_id=t.id
        WHERE
            t.registered_at BETWEEN ? AND ?
            AND a.id = ?
            AND t.type IN ($typeIn)
        GROUP BY a.id
        ", [
            $period->start, $period->end,
            $this->id,
        ]);

        $accumulationOfTransactionInPeriod = 0;
        if(count($accountResult) == 0)
        {
            $accumulationOfTransactionInPeriod = 0;
        }
        else
        {
            $accumulationOfTransactionInPeriod = $accountResult[0]->summary_value;
        }

        $result = $accumulationOfTransactionInPeriod + $period->getOpeningBalance($this);
        return $result;
    }

    public static function getAccountsSummaryArray(
        $argStartDateTime,
        $argEndDateTime,
        $argAccountType,
        $argAccountIdLike = '%',
        $argIsIncludeZeroAccounts = true,
        $argTransactionTypeArray = null
    ){
        if($argTransactionTypeArray == null)
        {
            $argTransactionTypeArray = ["'NORMAL'", "'CORRECTION'", "'ADJUSTMENT'"];
        }
        $typeIn = implode(", ", $argTransactionTypeArray);

        $query = "";
        $arrArgs = [];
        if($argIsIncludeZeroAccounts == true)
        {
            $query = "
                SELECT
                    a.id AS account_id,
                    IFNULL(tbl_detail.summary_value, 0) AS summary_value
                FROM accounts a
                LEFT JOIN
                (
                    SELECT
                        a.id AS account_id,
                        -- a.name AS account_name,
                        -- a.type AS account_type,
                        -- a.normal_balance_position AS account_normal_balance_position,
                        SUM(IF(td.taccount = 'DEBIT', td.value, -1 * td.value)) AS summary_value
                    FROM
                        accounts a
                        INNER JOIN transaction_details td ON td.account_id=a.id
                        INNER JOIN transactions t ON td.transaction_id=t.id

                    WHERE
                        a.type = ?
                        AND t.registered_at BETWEEN ? AND ?
                        AND a.id LIKE ?
                        AND t.type IN ($typeIn) -- agar tidak di-close berkali-kali
                    GROUP BY a.id
                ) tbl_detail ON tbl_detail.account_id=a.id
                WHERE
                    a.type = ?
                    AND a.id LIKE ?
            ";
            $arrArgs = [
                $argAccountType,
                $argStartDateTime, $argEndDateTime,
                $argAccountIdLike,

                $argAccountType,
                $argAccountIdLike,
            ];
        }
        else
        {
            $query = "
                SELECT
                    a.id AS account_id,
                    -- a.name AS account_name,
                    -- a.type AS account_type,
                    -- a.normal_balance_position AS account_normal_balance_position,
                    SUM(IF(td.taccount = 'DEBIT', td.value, -1 * td.value)) AS summary_value
                FROM
                    accounts a
                    INNER JOIN transaction_details td ON td.account_id=a.id
                    INNER JOIN transactions t ON td.transaction_id=t.id
                WHERE
                    a.type = ?
                    AND t.registered_at BETWEEN ? AND ?
                    AND a.id LIKE ?
                    AND t.type IN ($typeIn) -- agar tidak di-close berkali-kali
                GROUP BY a.id
            ";
            $arrArgs = [
                $argAccountType,
                $argStartDateTime, $argEndDateTime,
                $argAccountIdLike,
            ];
        }

        $accountsResult = DB::select($query, $arrArgs);

        return $accountsResult;
    }

}
