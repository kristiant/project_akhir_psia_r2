<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB; // uncomment kalau error
use Exception; // uncomment kalau error
use Config;

class Period extends Model
{
    protected $table = 'periods';
	public $timestamps = false; // apakah di tabel ada timestamps
    public function periodBalanceAccounts()
    {
        return $this->belongsToMany('App\Account', 'period_balances', 'period_id', 'account_id')
            ->withPivot('opening_balance','closing_balance')
        ;
    }

    public function getTransactions()
    {
        return Transaction::whereBetween('registered_at', [$this->start, $this->end] )
            ->orderBy('registered_at', 'asc')
            ->get();
    }

    public function getTransactionDetails($argFilterAccount)
    {
        return DB::table('transactions')
            ->join('transaction_details', 'transactions.id', '=', 'transaction_details.transaction_id')
            ->whereBetween('transactions.registered_at', [$this->start, $this->end] )
            ->where('transaction_details.account_id', '=', $argFilterAccount->id )
            ->orderBy('transactions.registered_at', 'asc')
            ->select('transactions.registered_at', 'transactions.notes', 'transaction_details.*')
            ->get();
    }

    private function closePeriodHelper_zeroAnAccount(
        $argDateTime,
        $argStrAccountTypeToBeZeroed,
        $argAccountToBeBalancedTo,
        $accountPrive = null,
        $argTAccountPositionDefaultForBalancer = "CREDIT"
    ){
        // ex: mengenolkan income:
        // $accountSummary = Account::where('type', '=', 'SUMMARY')->first();
        // $now = date('Y-m-d H:i:s');
        // closePeriodHelper_zeroAnAccount($now, "INCOMES", $accountSummary);
        //
        // note: closePeriodHelper_zeroAnAccount($now, FROM, TO);
        // note: closePeriodHelper_zeroAnAccount($now, FROM, TO, FROM_SPECIFIC);

        if($accountPrive != null)
        {
            // dipakai kalau mau zeroing prive saja
            $accountIdLike = $accountPrive->id;
        }
        else
        {
            $accountIdLike = '%';
        }

        // ======================================================================
        // ========== BEGIN ZEROING ==========
        // ======================================================================

        $accountsToBeZeroed = Account::getAccountsSummaryArray(
            $this->start,
            $this->end,
            $argStrAccountTypeToBeZeroed,
            $accountIdLike,
            true,
            ["'NORMAL'", "'CORRECTION'", "'ADJUSTMENT'", "'CLOSING'"]
        );
        // cari untuk buat detil transaksi "closing, zero income"
        // asumsi value jika (+) berarti DEBIT, jika (-) berarti KREDIT
        // jangan lupa bisa saja ada koreksi

        $summaryValueAllAccounts = 0;

        $tZeroing = new Transaction();
        $tZeroing->registered_at = $argDateTime;
        $tZeroing->notes = "Zero-ing " . $argStrAccountTypeToBeZeroed;
        $tZeroing->type = "CLOSING";
        $tZeroing->save();

        foreach ($accountsToBeZeroed as $key => $value)
        {
            $value->summary_value = -1 * $value->summary_value; // agar dibalik
            $taccountTd = 0;
            $valueTd = 0;
            $summaryValueAllAccounts += $value->summary_value;
            if($value->summary_value > 0)
            {
                $valueTd = $value->summary_value;
                $taccountTd = "DEBIT";
            }
            else if($value->summary_value < 0)
            {
                $valueTd = abs($value->summary_value);
                $taccountTd = "CREDIT";
            }
            else // value == 0 or -0
            {
                $valueTd = 0.0; // prevent -0
                $currentAccountToBeZeroed = Account::find($value->account_id);
                if($currentAccountToBeZeroed->normal_balance_position == "DEBIT") // dibalik posisinya
                {
                    $taccountTd = "CREDIT";
                }
                else
                {
                    $taccountTd = "DEBIT";
                }
            }

            /*
            //$tDtlZeroing = new TransactionDetail();
            //$tDtlZeroing->transaction_id = $tZeroing->id; // transaksi yang mana
            //$tDtlZeroing->account_id = $value->account_id; // akun mana
            //$tDtlZeroing->taccount = $taccountTd; // DEBIT/CREDIT
            //$tDtlZeroing->value = $valueTd; // berapa jumlahnya
            //$tDtlZeroing->save();
            */
            $tZeroing->accounts()->attach($value->account_id, ['taccount'=>$taccountTd,'value'=>$valueTd]);
        }


        $valueTd = 0;
        $taccountTd = 0;
        $summaryValueAllAccounts = -1 * $summaryValueAllAccounts; // tandanya di-balik agar jika semua tadi di credit, maka yang masuk ke summary account harus debit
        if($summaryValueAllAccounts > 0)
        {
            $valueTd = $summaryValueAllAccounts;
            $taccountTd = "DEBIT";
        }
        else if($summaryValueAllAccounts < 0)
        {
            $valueTd = abs($summaryValueAllAccounts);
            $taccountTd = "CREDIT";
        }
        else // value == 0 or -0
        {
            $valueTd = 0.0; // prevent -0
            $taccountTd = $argTAccountPositionDefaultForBalancer;
        }
        /*
        //$tDtlZeroIncomeIkhtisarLabaRugi = new TransactionDetail();
        //$tDtlZeroIncomeIkhtisarLabaRugi->transaction_id = $tZeroing->id; // transaksi yang mana
        //$tDtlZeroIncomeIkhtisarLabaRugi->account_id = $argAccountToBeBalancedTo->id; // akun mana
        //$tDtlZeroIncomeIkhtisarLabaRugi->taccount = $taccountTd; // DEBIT/CREDIT
        //$tDtlZeroIncomeIkhtisarLabaRugi->value = $valueTd; // berapa jumlahnya
        //$tDtlZeroIncomeIkhtisarLabaRugi->save();
        */
        $tZeroing->accounts()->attach($argAccountToBeBalancedTo->id, ['taccount'=>$taccountTd, 'value'=>$valueTd]);

        // ======================================================================
        // ========== END ZERO INCOME ==========
        // ======================================================================
    }

    public function closePeriod($argDateTime = null)
    {
        if($argDateTime == null)
        {
            $argDateTime = date('Y-m-d H:i:s');
        }

        $accSummary = Account::where('type', '=', 'SUMMARY')->first();

        $this->end = $argDateTime;

        // nol-kan income
        // nol-kan expense
        // nol-kan prive (kurangkan ke equity)
        // masukkan income summary ke equity

        // nol-kan income (INCOMES ACCOUNTS => SUMMARY)
        $this->closePeriodHelper_zeroAnAccount($argDateTime, "INCOMES", $accSummary, null, "CREDIT");
        // nol-kan expense (EXPENSES ACCOUNTS => SUMMARY)
        $this->closePeriodHelper_zeroAnAccount($argDateTime, "EXPENSES", $accSummary, null, "DEBIT");



        $accPrive = Account::find(302); //Account::find(Config::get('account.prive_id'));
        $accCapital = Account::find(301); //Account::find(Config::get('account.capital_id'));

        // masukkan income summary ke equity (SUMMARY => CAPITAL)
        $this->closePeriodHelper_zeroAnAccount($argDateTime, "SUMMARY", $accCapital, $accSummary, "CREDIT");

        // nol-kan prive (kurangkan ke equity) (PRIVE => CAPITAL)
        $this->closePeriodHelper_zeroAnAccount($argDateTime, "EQUITY", $accCapital, $accPrive, "DEBIT");


        $this->is_closed = true;
        $this->is_active = false;
        
        $this->save();


        // masukkan nilai closing value ke PeriodBalance
        $accountAll = Account::all();
        foreach ($accountAll as $key => $account)
        {
            $closingBalance = $account->getAccumulationOfAllTransactionInPeriod($this,
                ["'NORMAL'", "'CORRECTION'", "'ADJUSTMENT'", "'CLOSING'"]
            );
            $this->setClosingBalance($account, $closingBalance);
        }

    }

    public function generateReports()
    {
        $result = [];

        // ======================================================================
        // ========== BEGIN GENERATING INCOME STATEMENT ==========
        // ======================================================================
        $income_statement = [];

        $income_statement["Incomes"] = [];
        $incomeDetails = Account::getAccountsSummaryArray(
            $this->start,
            $this->end,
            'INCOMES'
        );
        //dd($incomeDetails); exit();
        foreach ($incomeDetails as $key => $incomeDetail)
        {
            $account = Account::find($incomeDetail->account_id);
            $income_statement["Incomes"][] = [
                "Account" => $account,
                "Value" => $incomeDetail->summary_value
            ];
        }

        $income_statement["Expenses"] = [];
        $expenseDetails = Account::getAccountsSummaryArray(
            $this->start,
            $this->end,
            'EXPENSES'
        );
        foreach ($expenseDetails as $key => $expenseDetail)
        {
            $account = Account::find($expenseDetail->account_id);
            $income_statement["Expenses"][] = [
                "Account" => $account,
                "Value" => $expenseDetail->summary_value
            ];
        }

        $income_statement["Totals"] = [];
        $income_statement["Totals"]["Incomes"] = 0;
        $income_statement["Totals"]["Expenses"] = 0;
        $income_statement["Totals"]["Income/Expense"] = 0;
        foreach ($income_statement["Incomes"] as $key => $value)
        {
            $income_statement["Totals"]["Incomes"] += $value["Value"];
        }
        foreach ($income_statement["Expenses"] as $key => $value)
        {
            $income_statement["Totals"]["Expenses"] += $value["Value"];
        }
        $income_statement["Totals"]["Income/Expense"] = $income_statement["Totals"]["Incomes"] + $income_statement["Totals"]["Expenses"];




        // ======================================================================
        // ========== BEGIN GENERATING STATEMENT OF OWNER'S EQUITY ==========
        // ======================================================================
        $equity_statement = [];
        $accPrive = Account::find(302); //Account::find(Config::get('account.prive_id'));
        $accCapital = Account::find(301); //Account::find(Config::get('account.capital_id'));

        $equity_statement["OpeningEquity"] = $this->getOpeningBalance($accCapital);
        $equity_statement["Income/Expense"] = -1 * $income_statement["Totals"]["Income/Expense"]; // dikali -1 supaya (+) jika untung dan (-) jika rugi
        $priveTransactionDetails = Account::getAccountsSummaryArray(
            $this->start,
            $this->end,
            'EQUITY',
            $accPrive->id
        );
        if(count($priveTransactionDetails) == 0)
        {
            $equity_statement["Prive"] = 0;
        }
        else
        {
            $equity_statement["Prive"] = $priveTransactionDetails[0]->summary_value;
        }



        // ======================================================================
        // ========== BEGIN GENERATING BALANCE SHEET ==========
        // ======================================================================
        $balance_sheet = [];
        $balance_sheet["Totals"] = [];
        $balance_sheet["Totals"]["Assets"] = 0.0;
        $balance_sheet["Totals"]["Liabilities"] = 0.0;
        $balance_sheet["Totals"]["Equity"] = 0.0;

        $depreciatingAssetsId = [];
        $depreciationAssetsId = [];
        $depreciatingAssetsIdAsKey = [];
        //$depreciations = //Config::get('account.depreciations');
        $depreciations = [
            [
                'asset' => 110,
                'depreciation' => 111,
            ],
        ];

        foreach ($depreciations as $key => $depreciating)
        {
            $depreciatingAssetsId[] = $depreciating["asset"];
            $depreciationAssetsId[] = $depreciating["depreciation"];
            $depreciatingAssetsIdAsKey[$depreciating["asset"]] = $depreciating["depreciation"];
        }

        $balance_sheet["Assets"] = [];
        $balance_sheet["Assets"]["Normal"] = [];
        $balance_sheet["Assets"]["Depreciating"] = [];
        $assetDetails = Account::getAccountsSummaryArray(
            $this->start,
            $this->end,
            'ASSETS'
        );
        //dd($assetDetails); exit();
        //dd($depreciatingAssetsId); exit();
        foreach ($assetDetails as $key => $assetDetail)
        {
            $account = Account::find($assetDetail->account_id);
            $openingBalance = $this->getOpeningBalance($account);
            if($account->normal_balance_position == "CREDIT")
            {
                $openingBalance = -1 * abs($openingBalance);
            }
            else
            {
                $openingBalance = abs($openingBalance);
            }
            $valueOnAccount = $assetDetail->summary_value + $openingBalance;
            if( in_array($assetDetail->account_id, $depreciatingAssetsId) )
            {
                // depreciating asset
                $accountDepreciationID = $depreciatingAssetsIdAsKey[$account->id];
                $accountDepreciation = Account::find($accountDepreciationID);

                $depreciationValue = 0;
                foreach ($assetDetails as $keyInner => $assetDetailInner)
                {
                    if($accountDepreciation->id == $assetDetailInner->account_id)
                    {
                        $depreciationValue = $assetDetailInner->summary_value;
                    }
                }

                $depreciationOpeningBalance = $this->getOpeningBalance($accountDepreciation);
                if($accountDepreciation->normal_balance_position == "CREDIT")
                {
                    $depreciationOpeningBalance = -1 * abs($depreciationOpeningBalance);
                }
                else
                {
                    $depreciationOpeningBalance = abs($depreciationOpeningBalance);
                }
                $depreciationValue += $depreciationOpeningBalance;
                $bookValue = $valueOnAccount + $depreciationValue; // ditambahkan karena yang depreciation angkanya minus
                $balance_sheet["Assets"]["Depreciating"][] = [
                    "Asset" => $account,
                    "Depreciation" => $accountDepreciation,
                    "AssetValue" => $valueOnAccount,
                    "DepreciationValue" => $depreciationValue,
                    "BookValue" => $bookValue,
                ];
                $balance_sheet["Totals"]["Assets"] += $bookValue;
            }
            else if( in_array($assetDetail->account_id, $depreciationAssetsId) )
            {
                // do nothing
            }
            else
            {
                // non depreciating asset
                $balance_sheet["Assets"]["Normal"][] = [
                    "Account" => $account,
                    "Value" => $valueOnAccount
                ];
                $balance_sheet["Totals"]["Assets"] += $valueOnAccount;
            }
        }


        $balance_sheet["Liabilities"] = [];
        $liabilityDetails = Account::getAccountsSummaryArray(
            $this->start,
            $this->end,
            'LIABILITIES'
        );
        foreach ($liabilityDetails as $key => $liabilityDetail)
        {
            $account = Account::find($liabilityDetail->account_id);
            $openingBalance = $this->getOpeningBalance($account);
            if($account->normal_balance_position == "CREDIT")
            {
                $openingBalance = -1 * abs($openingBalance);
            }
            else
            {
                $openingBalance = abs($openingBalance);
            }
            $valueOnAccount = $liabilityDetail->summary_value + $openingBalance;
            $balance_sheet["Liabilities"][] = [
                "Account" => $account,
                "Value" => $valueOnAccount
            ];
            $balance_sheet["Totals"]["Liabilities"] += $valueOnAccount;
        }


        $balance_sheet["Equity"] = [];

        $closingCapital = $equity_statement["OpeningEquity"] + $equity_statement["Income/Expense"] + $equity_statement["Prive"];
        $closingCapital = -1 * abs($closingCapital); // karena saldo di CREDIT, maka tandanya (-)
        $balance_sheet["Equity"][] = [
            "Account" => $accCapital,
            "Value" => $closingCapital
        ];
        $balance_sheet["Totals"]["Equity"] = $closingCapital;






        $result["IncomeStatement"] = $income_statement;
        $result["EquityStatement"] = $equity_statement;
        $result["BalanceSheet"] = $balance_sheet;
        return $result;
    }

    public function getOpeningBalance($argAccount)
    {
        if($this->periodBalanceAccounts()->where('accounts.id','=',$argAccount->id)->count() == 1)
        {
            return $this->periodBalanceAccounts()->where('accounts.id','=',$argAccount->id)->first()->pivot->opening_balance;
        }
        else
        {
            return 0;
        }
    }

    public function getClosingBalance($argAccount)
    {
        if($this->periodBalanceAccounts()->where('accounts.id','=',$argAccount->id)->count() == 1)
        {
            $closingBalance = $this->periodBalanceAccounts()->where('accounts.id','=',$argAccount->id)->first()->pivot->closing_balance;
            if($closingBalance == null)
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }

    public function setClosingBalance($argAccount, $argClosingBalance)
    {
        if($this->periodBalanceAccounts()->where('accounts.id','=',$argAccount->id)->count() == 0)
        {
            $this->periodBalanceAccounts()->attach($argAccount, ['opening_balance'=>null, 'closing_balance'=>$argClosingBalance]);
        }
        else
        {
            $this->periodBalanceAccounts()->updateExistingPivot($argAccount->id, ['closing_balance'=>$argClosingBalance]);
        }
    }

    public static function getCurrentPeriod()
    {
        $query = Period::where('is_active', '=', 1)
            ->get();
        if($query->count() != 1)
        {
            throw new Exception("Multiple active period detected!", 1);
        }
        else
        {
            return $query[0];
        }
    }





}
