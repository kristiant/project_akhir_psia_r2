# How To Use Project PSIA KJV

## Installation
please refer to file ```how to install.md``` for installation manual, make sure all the steps are completed before reading further

## Importing database
Import mysql dump (.sql file) to local database from diretory ```database/mysqldumps/``` with phpmyadmin or with mysql command line utility

## Running Server
run command ```php artisan serve``` on command line until these output are shown:

```Laravel development server started: <http://127.0.0.1:8000>```

## Login
- point browser to "http://localhost:8000/admin/login"
- login with these credentials: email: admin@crudbooster.com, password: 123456

## Sistem Informasi
Dashboard: http://localhost:8000/admin

### CRUD (Create-Retrieve-Update-Delete) Buat-Lihat-Ubah-Hapus Master Sistem Informasi
- Barang: http://localhost:8000/admin/barangs
- Bank: http://localhost:8000/admin/banks
- Supplier: http://localhost:8000/admin/suppliers
- Aset: http://localhost:8000/admin/asets

### Input Transaksi
- Transaksi Jual Barang http://localhost:8000/admin/notajualbarangs
- Transaksi Beli Barang http://localhost:8000/admin/notabelibarangs
- Transaksi Beli Aset
http://localhost:8000/admin/notabeliasets (note: aset tersebut harus didaftarkan dulu di master aset sebelum dibeli)
- Transaksi Menyewa Properti http://localhost:8000/admin/sewapropertis
- Transaksi Pembayaran Biaya http://localhost:8000/admin/notabayarbiayas (note: contoh biaya adalah seperti biaya listrik, biaya transportasi, bukan pelunasan hutang)
- Transaksi Pelunasan Hutang / Pelunasan Pembelian http://localhost:8000/admin/pelunasanbeli (note: untuk melunasi pembelian barang yang dibeli secara kredit dan untuk melunasi pembelian aset yang dicicil dari bank)
- Transaksi Penarikan Piutang Kartu Kredit http://localhost:8000/admin/draw/cc

## Akuntansi

### Lihat Laporan Akuntansi
http://localhost:8000/admin/report
- Laporan laba rugi
- Laporan perubahan ekuitas
- Laporan neraca
- Jurnal
- Buku Besar
- Worksheet
- Laporan Kartu Stok (bukan laporan akuntansi)

### Tutup Periode Akuntansi
http://localhost:8000/admin/period/close
- Pilih periode yang ingin ditutup
- Klik tutup
- Program akan secara otomatis mengenolkan pendapatan, prive, dan pengeluaran, serta menambahkannya ke modal pemilik 

### Input Transaksi Secara Manual
http://localhost:8000/admin/transactions
- Menginputkan transaksi secara manual atau melihat detil transaksi
- Kadang dibutuhkan untuk input transaksi secara manual (misalnya saat penyesuaian)