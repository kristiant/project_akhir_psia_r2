Period
-properties:
--id
--start
--end
--comment
--is_closed
--is_active // dalam semua period, hanya 1 yang boleh is_active nya TRUE
-methods:
--getTransactions()
	return array of OBJECT OF TYPE Transaction
	tinggal return Transaction::whereBetween('registered_at', $this->start, $this->end); 
--closePeriod()
	lakukan yang mengenolkan income, expense, prive, masukkan laba/rugi ke equity
--generateReports()
	jika belum di close maka tidak mau dihitung!! / throw exception!!
	return associative array / dictionary dengan struktur berikut:
		[
			"IncomeStatement" =>    // disini value: jika debit (+), jika credit (-)
			[
				"Incomes" => 
				[
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					...
				],
				"Expenses" => 
				[
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					...
				],
				"Totals" => =>
				[
					"Incomes" => 9999999,
					"Expenses" => 99999999,
				],
			],
			"EquityStatement" =>    // disini semua value (+)
			[
				"OpeningEquity" => 123,
				"Income/Expense" => 99,
				"Prive" => 2,
			],
			"BalanceSheet" =>
			[
				"Assets" => 
				[
					"Normal" =>    // disini value: jika debit (+), jika credit (-)
					[
						[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
						[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
						[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
						...
					],
					"Depreciating" =>    // disini value: jika debit (+), jika credit (-)
					[
						[ 
							"Asset" => OBJECT OF TYPE Account, 
							"Depreciation" => OBJECT OF TYPE Account,
							"AssetValue" => 1000,
							"DepreciationValue" => -200,
							"BookValue" => 800,
						],
						[ 
							"Asset" => OBJECT OF TYPE Account, 
							"Depreciation" => OBJECT OF TYPE Account,
							"AssetValue" => 1000,
							"DepreciationValue" => -200,
							"BookValue" => 800,
						],
						[ 
							"Asset" => OBJECT OF TYPE Account, 
							"Depreciation" => OBJECT OF TYPE Account,
							"AssetValue" => 1000,
							"DepreciationValue" => -200,
							"BookValue" => 800,
						],
						...
					],
				]
				"Liabilities" =>    // disini value: jika debit (+), jika credit (-)
				[
					[ "Account" => OBJECT OF TYPE Account, "Value" => -99999 ],
					[ "Account" => OBJECT OF TYPE Account, "Value" => -99999 ],
					[ "Account" => OBJECT OF TYPE Account, "Value" => -99999 ],
					...
				],
				"Equity" =>
				[
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					[ "Account" => OBJECT OF TYPE Account, "Value" => 99999 ],
					...
				],
				"Totals" => 
				[
					"Assets" => 9999999,
					"Liabilities" => 9999999,
					"Equity" = 9999999,
				],
			],
			"CashFlowStatement" => // note: berlaku untuk cash, rekening bank ABC, rekening bank XYZ
			[
				"OpeningCash" => 11111111,
				"Operational" =>
				[
					"Income" => 9999,
					"Expense" => -8888,
				],
				"Asset" =>
				[
					"Buy" => -7777,
					"Sell" => 6666,
				],
				"Owner" =>
				[
					"Deposit" => 5555,
					"Prive" => 4444,
				],
				"ClosingCash" => 222222222,
				"CashFlow" => 211111111, // dapat dari ClosingCash-OpeningCash, bisa minus
			],
		]
--getOpeningBalance($argAccount)
	return DOUBLE
	$cari = PeriodBalance::where('period_id', '=', $this->id)::where('account_id', '=', $argAccount->id)->get();
	if(count($cari) != 1) return 0 else return $cari[0];
-method-static:
--getCurrentPeriod()
	return OBJECT OF TYPE Period
