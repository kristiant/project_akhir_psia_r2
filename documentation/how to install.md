# HOW TO INSTALL

note: to view this document's markup, please use a markup editor (such as visual studio code) or read this as markdown in github/gitlab

## [INSTALL PREREQUISITIES]
1. install xampp for windows (php version minimum 7.1.13, mysql)
2. install composer (php dependency/package manager)
	Note: if normally installing didn't work, please read note on how to install and configure composer in http://kristian.ax.lt/url/shareubaya
3. install git for windows (optional, needed when pulling/cloning project from gitlab)

## [SYSTEM CONFIGURATIONS]
1. set php to environment variable PATH
	Note: this step is deemed success by: open cmd, type "php -v", then enter, it display the current version of php (need to be higher than 7.1.13)
2. set composer to environment variable PATH
	Note 1: this step is deemed success by: open cmd, type ```composer -V``` or ```composer.phar -V```, then enter, it display the current version of composer
	Note 2: if the output is not the version but composer's usage guide, then something went wrong, please refer to guide in http://kristian.ax.lt/url/shareubaya
3. set mysql to environment variable PATH (optional, needed when importing db dumps from mysql command line client)

## [PROJECT CONFIGURATIONS]
1. make sure mysql server (mysqld) is running and is accessible
2. create new database in mysql
3. set mysql connection string by copying ```.env.example``` to ```.env```, then edit it with notepad++ (change DB_DATABASE, DB_USERNAME, DB_PASSWORD)
4. import mysql dump to database from database/mysqldumps/
	Note: it can be done via phpmyadmin or via mysql command line client
5. open cmd, cd to project location, run command ```composer install``` or ```composer.phar install``` (need internet connection)
	Note: if the computer is behind proxy, please read note on how to set proxy for composer in http://kristian.ax.lt/url/shareubaya

## [CRUDBOOSTER CONFIGURATIONS]
1. open cmd, cd to project location, run command ```php artisan crudbooster:update```
	Note: this is to copy crudbooster controller and views

## [RUN PROJECT]
1. open cmd, cd to project location, run command ```php artisan serve```
2. to access modules created by crudbooster, open browser to url "http://localhost:8000/admin/login" (email:admin@crudbooster.com, pass: 123456)
3. to access other modules, open file routes/web.php, then open browser to url "http://localhost:8000" and follow the url in routes/web.php






#### IN DEPTH TUTORIAL

##### INSTALLING XAMPP
1. Download xampp installer for windows from https://www.apachefriends.org/download.html
2. Pick version that match the requirement (php minimum version 7.1.13, recommended 7.2)
3. Run installer as administrator
4. Remember / take note of mysql password when installing

##### CONFIGURING 'PATH' VARIABLES
What is PATH variable? It is an enviroment variable that indicate where is the path of an executable that can be executed directly from the command line (Windows Command Prompt)

Example: 
```PATH=C:\Windows\System32```
That means that if the user open command prompt and typed 'notepad' then hit enter key, the command prompt will look for program called 
```C:\Windows\System32\notepad.exe```
and run it. 

1. Open control panel > System and security > System > Advanced system settings (admin)
2. OR Open control panel > type 'environment' > select 'edit the system environment variables'
3. Advanced (tab) > Environment variables > select 'PATH' in the bottom listbox > click 'edit' button below the bottom listbox
4. If the system is running windows10, click 'new', then type the complete path to be added
5. If the system is running below windows10, type the complete path to the dialogbox that appeared (make sure to use ';' as delimiter between the paths)
6. To test if the path has been successfully inserted, open command prompt and type the name of any program that can be opened inside the newly added path and press enter. If the program run (not error saying 'is not recognized ...') then the PATH variable has been added successfully 

##### INSTALLING GIT BASH
1. Download git installer from https://git-scm.com/downloads
2. Pick download for windows
3. In Installation wizard, configure to use sublime, notepad++, or nano as default editor (don't select vi or vim if you're a beginner)
4. In installation wizard, select yes when asked if you want to integrate git to right click context menu

##### INSTALLING COMPOSER
1. Make sure you have php installed in your computer before doing this step (see installing xampp if you're running windows), test it by opening command prompt and type 'php -v' then enter
2. Go to the PHP installation path (usually in xampp windows it is in C:\xampp\php) inside command line (cd C:\xampp\php)
3. Go to https://getcomposer.org/download/
4. Copy paste the instruction there (or just copy paste below) into the command prompt / terminal (note: it's recommended to copy paste from composer website rather than here)
```php
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```
5. Test the composer installation by running command 'composer -V' (if it displays the version only then installation success)
6. If the command in step 5 do not display the version only (it's output should only be 1-2 lines), then see guide below

##### WINDOWS WORKAROUND FOR UNABILITY TO RUN PHAR FILE (GO HERE IF COMPOSER INSTALLATION FAILED)
1. Add the file's directory to PATH variable
2. Do not add ```.phar``` extension to PATHEXT variable
3. Create a new text file with the same name with .php file, but with .bat extension
4. Edit the .bat file, change its content to
```bat
@ECHO OFF
setlocal DISABLEDELAYEDEXPANSION
SET BIN_TARGET=%~dp0/php_or_phar_file_name_here
php "%BIN_TARGET%" %*
```

Example: file name = c:/xampp/composer/composer.phar
```bat
@ECHO OFF
setlocal DISABLEDELAYEDEXPANSION
SET BIN_TARGET=%~dp0/composer.phar
php "%BIN_TARGET%" %*
```
5. Change the text ```php_or_phar_file_name_here``` with the file name of the ```.phar``` file (with extension)