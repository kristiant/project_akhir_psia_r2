1. pada child table (contohnya pada period, yang menentukan isi period_balances / starting balance tiap account)
	isi (row) pada child table tidak bisa diisi null / tidak diisi (jika tidak diisi error)
	for some reason, kadang bisa kadang tidak bisa
2. pada crud form biasa, jika ada field yang tipenya money, harus diisi 0 (tidak boleh dikosongi)
	menyebabkan error number_format php
3. pada crud yang punya child table, jika di delete/edit kadang error, jika add kadang error juga jika tidak ada anggota dari child tablenya