$(document).ready(function()
{
	hideAllInputs();

	$('#detailbarangharga').attr('placeholder', '(dalam Rupiah)');

	$('#biaya').val(0);
	$('#durasi_sewa_bulan').val(12);

	$('#form-group-bank_id').hide();
	$('#form-group-discount_term').hide();
	$('#form-group-nomor_rekening').hide();
});

$(document).on('click', '.applyBtn', function()
{
	showAllInputs();
});

$('#cara_bayar').on('change', function()
{
	if($('#cara_bayar').val() == "Kredit") {
		$('#form-group-bank_id').hide();
		$('#form-group-discount_term').show();
		$('#form-group-nomor_rekening').hide();
	}
	else if($('#cara_bayar').val() == "Tunai Transfer") {
		$('#form-group-bank_id').show();
		$('#form-group-discount_term').hide();
		$('#form-group-nomor_rekening').show();
	}
	else {
		$('#form-group-bank_id').hide();
		$('#form-group-discount_term').hide();
		$('#form-group-nomor_rekening').hide();
	}
});

$('#biaya').focusout(function()
{
	if($('#biaya').val() == '' || $('#biaya').val() < 0)
	{
		$('#biaya').val(0);
	}
});

$('#durasi_sewa_bulan').focusout(function()
{
	if($('#durasi_sewa_bulan').val() == '' || $('#durasi_sewa_bulan').val() < 0)
	{
		$('#durasi_sewa_bulan').val(0);
	}
});

function hideAllInputs()
{
	$('#form-group-nama').hide();
	$('#form-group-alamat').hide();
	$('#form-group-biaya').hide();
	$('#form-group-durasi_sewa_bulan').hide();
	$('#form-group-cara_bayar').hide();
}

function showAllInputs()
{
	$('#form-group-nama').show();
	$('#form-group-alamat').show();
	$('#form-group-biaya').show();
	$('#form-group-durasi_sewa_bulan').show();
	$('#form-group-cara_bayar').show();
}