$(document).ready(function()
{
	hideAllInputs();

	$('#detailbarangharga').attr('placeholder', '(dalam Rupiah)');

	$('#nominal').val(0);

	$('#form-group-bank_id').hide();
	$('#form-group-nomor_rekening').hide();
});

$(document).on('click', '.applyBtn', function()
{
	showAllInputs();
});

$('#cara_bayar').on('change', function()
{
	if($('#cara_bayar').val() == "Kredit") {
		$('#form-group-bank_id').hide();
		$('#form-group-discount_term').show();
		$('#form-group-nomor_rekening').hide();
	}
	else if($('#cara_bayar').val() == "Tunai Transfer") {
		$('#form-group-bank_id').show();
		$('#form-group-discount_term').hide();
		$('#form-group-nomor_rekening').show();
	}
	else {
		$('#form-group-bank_id').hide();
		$('#form-group-discount_term').hide();
		$('#form-group-nomor_rekening').hide();
	}
});

function hideAllInputs()
{
	$('#form-group-keterangan').hide();
	$('#form-group-account_id').hide();
	$('#form-group-nominal').hide();
	$('#form-group-cara_bayar').hide();
}

function showAllInputs()
{
	$('#form-group-keterangan').show();
	$('#form-group-account_id').show();
	$('#form-group-nominal').show();
	$('#form-group-cara_bayar').show();
}