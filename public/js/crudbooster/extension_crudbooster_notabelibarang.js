$(document).ready(function()
{
	hideAllInputs();

	$('#detailbarangharga').attr('placeholder', '(dalam Rupiah)');

	$('#detailbarangjumlah').val(0);
	$('#detailbarangharga').val(0);

	$('#biaya_kirim').prop('disabled', true);
	$('#biaya_kirim').val(0);

	$('#form-group-bank_id').hide();
	$('#form-group-discount_term').hide();
	$('#form-group-nomor_rekening').hide();
});

$(document).on('click', '.applyBtn', function()
{
	showAllInputs();
});

$('#fob').on('change', function()
{
	if($('#fob').val() == "FOB Shipping Point") {
		$('#biaya_kirim').prop('disabled', false);
		$('#biaya_kirim').val(0);
	}
	else {
		$('#biaya_kirim').prop('disabled', true);
		$('#biaya_kirim').val(0);
	}
});

$('#cara_bayar').on('change', function()
{
	if($('#cara_bayar').val() == "Kredit") {
		$('#form-group-bank_id').hide();
		$('#form-group-discount_term').show();
		$('#form-group-nomor_rekening').hide();
	}
	else if($('#cara_bayar').val() == "Tunai Transfer") {
		$('#form-group-bank_id').show();
		$('#form-group-discount_term').hide();
		$('#form-group-nomor_rekening').show();
	}
	else {
		$('#form-group-bank_id').hide();
		$('#form-group-discount_term').hide();
		$('#form-group-nomor_rekening').hide();
	}
});

$('#detailbarangjumlah').focusout(function()
{
	if($('#detailbarangjumlah').val() == '' || $('#detailbarangjumlah').val() < 0)
	{
		$('#detailbarangjumlah').val(0);
	}
});

$('#detailbarangharga').focusout(function()
{
	if($('#detailbarangharga').val() == '' || $('#detailbarangharga').val() < 0)
	{
		$('#detailbarangharga').val(0);
	}
});

function hideAllInputs()
{
	$('#form-group-supplier_id').hide();
	$('#form-group-fob').hide();
	$('#form-group-biaya_kirim').hide();
	$('#form-group-cara_bayar').hide();
	$('#panel-form-detailbarang').hide()
}

function showAllInputs()
{
	$('#form-group-supplier_id').show();
	$('#form-group-fob').show();
	$('#form-group-biaya_kirim').show();
	$('#form-group-cara_bayar').show();
	$('#panel-form-detailbarang').show()
}