$(document).ready(function()
{
	hideAllInputs();

	$('#detailbarangharga').attr('placeholder', '(dalam Rupiah)');
	$('#detailbarangdiskon').attr('placeholder', 'per item (dalam Rupiah)');

	$('#detailbarangjumlah').val(0);
	$('#detailbarangharga').val(0);
	$('#detailbarangdiskon').val(0);

	$('#biaya_kirim').prop('disabled', true);
	$('#biaya_kirim').val(0);

	$('#form-group-bank_id').hide();
	$('#form-group-nomor_rekening').hide();
	$('#form-group-nomor_kartu_kredit').hide();
	$('#form-group-discount_term').hide();
});

$(document).on('click', '.applyBtn', function()
{
	showAllInputs();
});

$('#fob').on('change', function()
{
	if($('#fob').val() == "FOB Destination") {
		$('#biaya_kirim').prop('disabled', false);
		$('#form-group-biaya_kirim label').html("Biaya Kirim (Rupiah)");
		$('#biaya_kirim').val(0);
	}
	else {
		//$('#biaya_kirim').prop('disabled', true);
		$('#biaya_kirim').prop('disabled', false);
		$('#form-group-biaya_kirim label').html("Biaya Kirim (Rupiah) (Ditanggung pembeli)");
		$('#biaya_kirim').val(0);
	}
});

$('#cara_bayar').on('change', function()
{
	if($('#cara_bayar').val() == "Kartu Kredit") {
		$('#form-group-bank_id').show();
		$('#form-group-nomor_rekening').hide();
		$('#form-group-nomor_kartu_kredit').show();
		//$('#form-group-discount_term').show();
		$('#form-group-discount_term').hide();
	}
	else if($('#cara_bayar').val() == "Tunai Transfer") {
		$('#form-group-bank_id').show();
		$('#form-group-nomor_rekening').show();
		$('#form-group-nomor_kartu_kredit').hide();
		$('#form-group-discount_term').hide();
	}
	else {
		$('#form-group-bank_id').hide();
		$('#form-group-nomor_rekening').hide();
		$('#form-group-nomor_kartu_kredit').hide();
		$('#form-group-discount_term').hide();
	}
});

$('#detailbarangjumlah').focusout(function()
{
	if($('#detailbarangjumlah').val() == '' || $('#detailbarangjumlah').val() < 0)
	{
		$('#detailbarangjumlah').val(0);
	}
});

$('#detailbarangharga').focusout(function()
{
	if($('#detailbarangharga').val() == '' || $('#detailbarangharga').val() < 0)
	{
		$('#detailbarangharga').val(0);
	}
});

$('#detailbarangdiskon').focusout(function()
{
	if($('#detailbarangdiskon').val() == '' || $('#detailbarangdiskon').val() < 0)
	{
		$('#detailbarangdiskon').val(0);
	}
});

function hideAllInputs()
{
	$('#form-group-fob').hide();
	$('#form-group-biaya_kirim').hide();
	$('#form-group-cara_bayar').hide();
	$('#panel-form-detailbarang').hide()
}

function showAllInputs()
{
	$('#form-group-fob').show();
	$('#form-group-biaya_kirim').show();
	$('#form-group-cara_bayar').show();
	$('#panel-form-detailbarang').show()
}