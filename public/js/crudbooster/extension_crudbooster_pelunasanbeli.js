$(document).ready(function()
{
    $('#nominal').val(0);
    $('#biaya_transfer').val(0);

    hideAllInputs();

    $('#form-group-bank_id').hide();
    $('#form-group-nomor_rekening').hide();
    $('#form-group-biaya_transfer').hide();
});

$(document).on('click', '.applyBtn', function()
{
    showAllInputs();
});

$('#notabelibarang_id').on('change', function()
{
    if($('#notabelibarang_id').val() != "")
    {
        $('#form-group-notabeliaset_id').hide();
    }
    else if($('#notabelibarang_id').val() == "")
    {
        $('#form-group-notabeliaset_id').show();   
    }
});

$('#notabeliaset_id').on('change', function()
{
    if($('#notabeliaset_id').val() != "")
    {
        $('#form-group-notabelibarang_id').hide();
    }
    else if($('#notabeliaset_id').val() == "")
    {
        $('#form-group-notabelibarang_id').show();   
    }
});

$('#cara_bayar').on('change', function()
{
    if($('#cara_bayar').val() == "Tunai Transfer") {
        $('#form-group-bank_id').show();
        $('#form-group-nomor_rekening').show();
        $('#form-group-biaya_transfer').show();
    }
    else {
        $('#form-group-bank_id').hide();
        $('#form-group-nomor_rekening').hide();
        $('#form-group-biaya_transfer').hide();
    }
});

$('#nominal').focusout(function()
{
    var nominal = $('#nominal').val().replace(/,/g, '');
    var sisa = $('#labelsisa').text().split(' ')[1];
    console.log(nominal);
    console.log(sisa);
    if(nominal > sisa)
    {
        $('#nominal').val(sisa);
    }
});

function hideAllInputs()
{
    $('#form-group-notabelibarang_id').hide();
    $('#form-group-notabeliaset_id').hide();
    $('#form-group-keterangan').hide();
    $('#form-group-account_id').hide();
    $('#form-group-nominal').hide();
    $('#form-group-cara_bayar').hide();
}

function showAllInputs()
{
    $('#form-group-notabelibarang_id').show();
    $('#form-group-notabeliaset_id').show();
    $('#form-group-keterangan').show();
    $('#form-group-account_id').show();
    $('#form-group-nominal').show();
    $('#form-group-cara_bayar').show();
}

var sisaBelumDibayar = 0.0;

$("#notabelibarang_id").change(function(){
    ajaxTampilkanSisa(
        "notabelibarang_id", 
        "form-group-notabelibarang_id", 
        "/ajax/pelunasanbeli/sisahutang/barang/", 
        "notabeliaset_id"
    );

    /*
    var selected_id = $("#notabelibarang_id").val();
    if(isNaN(selected_id) == false && selected_id != null)
    {
        //alert("numeric");

        var ajaxUrl = "/ajax/pelunasanbeli/sisahutang/barang/" + selected_id.toString();
        console.log(ajaxUrl);
        $.ajax({
            type: "GET",
            url: ajaxUrl,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data)
            {
                console.log(data);
                varResultObj = JSON.parse(data); // assume the return will always be {"status" : "success"} OR {"status" : "fail"}
                if(varResultObj.status == "success")
                {
                    //
                    var sisa = varResultObj.sisa.toString();
                    $("#form-group-notabelibarang_id").append("<label class='control-label col-sm-2'>Sisa: " + sisa + "</label>");
                }
                else
                {
                    //alert("NOT FOUND");
                    console.log("ERROR : " + data);
                }
            },
            error: function (e) {
                alert("ERROR");
                console.log("ERROR : ", e);
            }
        });
    }
    */
});

$("#notabeliaset_id").change(function(){
    ajaxTampilkanSisa(
        "notabeliaset_id", 
        "form-group-notabeliaset_id", 
        "/ajax/pelunasanbeli/sisahutang/aset/", 
        "notabelibarang_id"
    );
    /*
    var selected_id = $("#notabeliaset_id").val();
    if(isNaN(selected_id) == false && selected_id != null)
    {
        var ajaxUrl = "/ajax/pelunasanbeli/sisahutang/aset/" + selected_id.toString();
        console.log(ajaxUrl);
        $.ajax({
            type: "GET",
            url: ajaxUrl,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data)
            {
                console.log(data);
                varResultObj = JSON.parse(data); // assume the return will always be {"status" : "success"} OR {"status" : "fail"}
                if(varResultObj.status == "success")
                {
                    //
                    var sisa = varResultObj.sisa.toString();
                    $("#form-group-notabeliaset_id").append("<label class='control-label col-sm-2'>Sisa: " + sisa + "</label>");
                }
                else
                {
                    //alert("NOT FOUND");
                    console.log("ERROR : " + data);
                }
            },
            error: function (e) {
                alert("ERROR");
                console.log("ERROR : ", e);
            }
        });
    }
    */
});

function ajaxTampilkanSisa(argIdSelect, argIdFormGroup, argUrl, argIdSelectToBeReset) // all arguments are string
{
    var selected_id = $("#" + argIdSelect).val();
    var tanggal = $("#tanggal").val();
    console.log(typeof selected_id);
    console.log(parseInt(selected_id))

    if(isNaN(parseInt(selected_id)) == false && tanggal != "")
    {
        //var ajaxUrl = "/ajax/pelunasanbeli/sisahutang/aset/" + selected_id.toString();
        var ajaxUrl = argUrl + selected_id.toString() + "/" + encodeURI(tanggal);
        console.log(ajaxUrl);
        $.ajax({
            type: "GET",
            url: ajaxUrl,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data)
            {
                console.log(data);
                varResultObj = JSON.parse(data); // assume the return will always be {"status" : "success"} OR {"status" : "fail"}
                if(varResultObj.status == "success")
                {
                    // reset controls
                    $(".labelsisa").remove();
                    $("#" + argIdSelectToBeReset).prop("selectedIndex", 0).change();

                    // display label
                    var sisa = varResultObj.sisa.toString();
                    sisaBelumDibayar = sisa;
                    var message = "Sisa: " + sisa;
                    if(typeof varResultObj.diskon != 'undefined')
                    {
                        message = message + ", (ada diskon sebesar: " + varResultObj.diskon.toString() + ")";
                    }
                    $("#" + argIdFormGroup).append(
                        "<label class='control-label col-sm-2 labelsisa' id='labelsisa'>" + 
                        message + 
                        "</label>");
                    $("#nominal").val(sisa);
                }
                else
                {
                    //alert("NOT FOUND");
                    console.log("ERROR : " + data);
                }
            },
            error: function (e) {
                alert("ERROR");
                console.log("ERROR : ", e);
            }
        });
    }
}