$(document).ready(function()
{
	hideAllInputs();

	$('#detailbarangharga').attr('placeholder', '(dalam Rupiah)');

	$('#harga_beli').val(0);
	$('#jumlah').val(0);

	$('#uang_muka_cicilan').val(0);
	$('#durasi_cicilan').val(12);

	$('#biaya_kirim').prop('disabled', true);
	$('#biaya_kirim').val(0);

	$('#form-group-bank_id').hide();
	$('#form-group-nomor_rekening').hide();
	$('#form-group-uang_muka_cicilan').hide();
	$('#form-group-durasi_cicilan').hide();
});

$(document).on('click', '.applyBtn', function()
{
	showAllInputs();
});

$('#fob').on('change', function()
{
	if($('#fob').val() == "FOB Shipping Point") {
		$('#biaya_kirim').prop('disabled', false);
		$('#biaya_kirim').val(0);
	}
	else {
		$('#biaya_kirim').prop('disabled', true);
		$('#biaya_kirim').val(0);
	}
});

$('#cara_bayar').on('change', function()
{
	if($('#cara_bayar').val() == "Cicilan") {
		$('#form-group-bank_id').hide();
		$('#form-group-nomor_rekening').hide();
		$('#form-group-uang_muka_cicilan').show();
		$('#form-group-durasi_cicilan').show();
	}
	else if($('#cara_bayar').val() == "Tunai Transfer") {
		$('#form-group-bank_id').show();
		$('#form-group-nomor_rekening').show();
		$('#form-group-uang_muka_cicilan').hide();
		$('#form-group-durasi_cicilan').hide();
	}
	else {
		$('#form-group-bank_id').hide();
		$('#form-group-nomor_rekening').hide();
		$('#form-group-uang_muka_cicilan').hide();
		$('#form-group-durasi_cicilan').hide();
	}
});

$('#jumlah').focusout(function()
{
	if($('#jumlah').val() == '' || $('#jumlah').val() < 0)
	{
		$('#jumlah').val(0);
	}
});

$('#durasi_cicilan').focusout(function()
{
	if($('#durasi_cicilan').val() == '' || $('#durasi_cicilan').val() < 0)
	{
		$('#durasi_cicilan').val(0);
	}
});

function hideAllInputs()
{
	$('#form-group-supplier_id').hide();
	$('#form-group-aset_id').hide();
	$('#form-group-cara_bayar').hide();
	$('#form-group-fob').hide();
	$('#form-group-biaya_kirim').hide();
	$('#form-group-harga_beli').hide();
	$('#form-group-jumlah').hide();
}

function showAllInputs()
{
	$('#form-group-supplier_id').show();
	$('#form-group-aset_id').show();
	$('#form-group-cara_bayar').show();
	$('#form-group-fob').show();
	$('#form-group-biaya_kirim').show();
	$('#form-group-harga_beli').show();
	$('#form-group-jumlah').show();
}