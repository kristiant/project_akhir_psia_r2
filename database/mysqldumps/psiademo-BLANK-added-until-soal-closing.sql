-- MySQL dump 10.16  Distrib 10.1.33-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: project_akhir_psia_r2
-- ------------------------------------------------------
-- Server version	10.1.33-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `normal_balance_position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (101,'Kas Di Tangan','ASSETS','DEBIT'),(102,'Rekening Bank ABC','ASSETS','DEBIT'),(103,'Rekening Bank XYZ','ASSETS','DEBIT'),(104,'Piutang Dagang','ASSETS','DEBIT'),(105,'Piutang Kartu Kredit','ASSETS','DEBIT'),(106,'Sewa Dibayar Di Muka','ASSETS','DEBIT'),(107,'Sediaan Stok','ASSETS','DEBIT'),(108,'Sediaan Habis Pakai','ASSETS','DEBIT'),(110,'Kendaraan','ASSETS','DEBIT'),(111,'Akumulasi Depresiasi Kendaraan','ASSETS','CREDIT'),(201,'Hutang Dagang','LIABILITIES','CREDIT'),(202,'Hutang Bank','LIABILITIES','CREDIT'),(301,'Modal Pemilik','EQUITY','CREDIT'),(302,'Prive','EQUITY','DEBIT'),(401,'Penjualan','INCOMES','CREDIT'),(402,'Diskon Penjualan','INCOMES','DEBIT'),(405,'Pendapatan Lain-Lain','INCOMES','CREDIT'),(501,'HPP','EXPENSES','DEBIT'),(506,'Biaya Gaji Karyawan','EXPENSES','DEBIT'),(507,'Biaya Sewa','EXPENSES','DEBIT'),(508,'Biaya Depresiasi Kendaraan','EXPENSES','DEBIT'),(509,'Biaya Utilitas','EXPENSES','DEBIT'),(515,'Rugi Penjualan Aset Tetap','EXPENSES','DEBIT'),(520,'Biaya/Rugi Lain-Lain','EXPENSES','DEBIT'),(901,'Income Summary','SUMMARY','DEBIT');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asets`
--

DROP TABLE IF EXISTS `asets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `asetscol` varchar(45) DEFAULT NULL,
  `usia` int(11) DEFAULT NULL,
  `tanggal_beli` datetime DEFAULT NULL,
  `account_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asets`
--

LOCK TABLES `asets` WRITE;
/*!40000 ALTER TABLE `asets` DISABLE KEYS */;
INSERT INTO `asets` VALUES (1,'Motor Roda 3 JiaLing',0,'0',0,'2018-11-10 00:00:00',110);
/*!40000 ALTER TABLE `asets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banks`
--

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;
INSERT INTO `banks` VALUES (1,'ABC'),(2,'XYZ'),(3,'Abal-Abal');
/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barangs`
--

DROP TABLE IF EXISTS `barangs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) DEFAULT NULL,
  `harga_jual` double DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `jumlah_stok_gudang` int(11) DEFAULT NULL,
  `jumlah_stok_jual` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barangs`
--

LOCK TABLES `barangs` WRITE;
/*!40000 ALTER TABLE `barangs` DISABLE KEYS */;
INSERT INTO `barangs` VALUES (1,'Tas Plastik Daur Ulang',0,14814.814814814814,90,0),(2,'Dompet Plastik Daur Ulang',0,4777.777777777777,150,0),(3,'Tas Perca',0,41000,45,0),(4,'Tempat Botol Perca',0,16000,90,0);
/*!40000 ALTER TABLE `barangs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_apicustom`
--

DROP TABLE IF EXISTS `cms_apicustom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_apicustom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_apicustom`
--

LOCK TABLES `cms_apicustom` WRITE;
/*!40000 ALTER TABLE `cms_apicustom` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_apicustom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_apikey`
--

DROP TABLE IF EXISTS `cms_apikey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_apikey` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_apikey`
--

LOCK TABLES `cms_apikey` WRITE;
/*!40000 ALTER TABLE `cms_apikey` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_apikey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_dashboard`
--

DROP TABLE IF EXISTS `cms_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_dashboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_dashboard`
--

LOCK TABLES `cms_dashboard` WRITE;
/*!40000 ALTER TABLE `cms_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_email_queues`
--

DROP TABLE IF EXISTS `cms_email_queues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_email_queues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_email_queues`
--

LOCK TABLES `cms_email_queues` WRITE;
/*!40000 ALTER TABLE `cms_email_queues` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_email_queues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_email_templates`
--

DROP TABLE IF EXISTS `cms_email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_email_templates`
--

LOCK TABLES `cms_email_templates` WRITE;
/*!40000 ALTER TABLE `cms_email_templates` DISABLE KEYS */;
INSERT INTO `cms_email_templates` VALUES (1,'Email Template Forgot Password Backend','forgot_password_backend',NULL,'<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>','[password]','System','system@crudbooster.com',NULL,'2018-11-10 02:42:56',NULL);
/*!40000 ALTER TABLE `cms_email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_logs`
--

DROP TABLE IF EXISTS `cms_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_logs`
--

LOCK TABLES `cms_logs` WRITE;
/*!40000 ALTER TABLE `cms_logs` DISABLE KEYS */;
INSERT INTO `cms_logs` VALUES (1,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-11-10 02:46:49',NULL),(2,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/add-save','Add New Data Tas Plastik Daur Ulang at Barang','',1,'2018-11-10 03:00:27',NULL),(3,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/add-save','Add New Data Dompet Plastik Daur Ulang at Barang','',1,'2018-11-10 03:00:49',NULL),(4,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/add-save','Add New Data Tas Perca at Barang','',1,'2018-11-10 03:01:00',NULL),(5,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/add-save','Add New Data Tempat Botol Perca at Barang','',1,'2018-11-10 03:01:11',NULL),(6,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/banks/add-save','Add New Data ABC at Bank','',1,'2018-11-10 03:01:28',NULL),(7,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/banks/add-save','Add New Data XYZ at Bank','',1,'2018-11-10 03:01:35',NULL),(8,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/banks/add-save','Add New Data Abal-Abal at Bank','',1,'2018-11-10 03:02:07',NULL),(9,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/suppliers/add-save','Add New Data UKM Kreatifitas Alam at Supplier','',1,'2018-11-10 03:02:30',NULL),(10,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/suppliers/add-save','Add New Data UKM Sejahtera Crafting at Supplier','',1,'2018-11-10 03:02:44',NULL),(11,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/asets/add-save','Add New Data Motor Roda 3 JiaLing at Aset','',1,'2018-11-10 03:03:33',NULL),(12,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabelibarangs/add-save','Add New Data  at Nota Beli Barang','',1,'2018-11-10 03:16:31',NULL),(13,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/sewapropertis/add-save','Add New Data Ruko at Sewa Properti','',1,'2018-11-10 03:19:38',NULL),(14,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabayarbiayas/add-save','Add New Data  at Nota Bayar Biaya','',1,'2018-11-10 03:26:21',NULL),(15,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/suppliers/add-save','Add New Data Other at Supplier','',1,'2018-11-10 03:29:08',NULL),(16,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabeliasets/add-save','Add New Data  at Nota Beli Aset','',1,'2018-11-10 03:32:07',NULL),(17,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notajualbarangs/add-save','Add New Data  at Nota Jual Barang','',1,'2018-11-10 03:52:21',NULL),(18,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notajualbarangs/add-save','Add New Data  at Nota Jual Barang','',1,'2018-11-10 04:12:02',NULL),(19,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-11-10 19:42:39',NULL),(20,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/menu_management/add-save','Add New Data SistemInformasi at Menu Management','',1,'2018-11-10 19:45:02',NULL),(21,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/menu_management/edit-save/11','Update data Sistem Informasi at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>SistemInformasi</td><td>Sistem Informasi</td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>',1,'2018-11-10 19:45:21',NULL),(22,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/menu_management/add-save','Add New Data Akuntansi at Menu Management','',1,'2018-11-10 19:47:33',NULL),(23,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/module_generator/delete/24','Delete data Transaction at Module Generator','',1,'2018-11-10 20:02:25',NULL),(24,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-11-10 23:59:38',NULL),(25,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/edit-save/4','Update data Tempat Botol Perca at Barang','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2018-11-11 00:15:15',NULL),(26,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/edit-save/3','Update data Tas Perca at Barang','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2018-11-11 00:15:27',NULL),(27,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notajualbarangs/add-save','Add New Data  at Nota Jual Barang','',1,'2018-11-11 00:16:24',NULL),(28,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabayarbiayas/add-save','Add New Data  at Nota Bayar Biaya','',1,'2018-11-11 01:56:32',NULL),(29,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/asets/edit-save/1','Update data Motor Roda 3 JiaLing at Aset','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>account_id</td><td></td><td>110</td></tr></tbody></table>',1,'2018-11-11 02:06:27',NULL),(30,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-11-11 17:28:04',NULL),(31,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabeliasets/edit-save/1','Update data  at Nota Beli Aset','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>uang_muka_cicilan</td><td></td><td>0</td></tr><tr><td>nomor_rekening</td><td></td><td></td></tr><tr><td>fob</td><td></td><td></td></tr><tr><td>biaya_kirim</td><td></td><td>0</td></tr><tr><td>resi_kirim</td><td></td><td></td></tr><tr><td>is_lunas</td><td></td><td></td></tr></tbody></table>',1,'2018-11-11 19:21:48',NULL),(32,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/login','admin@crudbooster.com login with IP Address 127.0.0.1','',1,'2018-11-12 05:20:03',NULL),(33,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabelibarangs/add-save','Add New Data  at Nota Beli Barang','',1,'2018-11-12 05:47:39',NULL),(34,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/transactions/action-selected','Delete data 3,2,1 at Transaction','',1,'2018-11-12 08:51:24',NULL),(35,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabeliasets/action-selected','Delete data 1 at Nota Beli Aset','',1,'2018-11-12 08:54:04',NULL),(36,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabayarbiayas/action-selected','Delete data 2,1 at Nota Bayar Biaya','',1,'2018-11-12 08:54:16',NULL),(37,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/sewapropertis/action-selected','Delete data 1 at Sewa Properti','',1,'2018-11-12 08:54:31',NULL),(38,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/edit-save/4','Update data Tempat Botol Perca at Barang','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>harga_beli</td><td>300</td><td>0</td></tr><tr><td>jumlah_stok_gudang</td><td>48</td><td>0</td></tr></tbody></table>',1,'2018-11-12 08:55:39',NULL),(39,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/edit-save/3','Update data Tas Perca at Barang','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>harga_beli</td><td>200</td><td>0</td></tr><tr><td>jumlah_stok_gudang</td><td>20</td><td>0</td></tr></tbody></table>',1,'2018-11-12 08:55:52',NULL),(40,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/barangs/edit-save/1','Update data Tas Plastik Daur Ulang at Barang','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>harga_beli</td><td>203000</td><td>0</td></tr><tr><td>jumlah_stok_gudang</td><td>1</td><td>0</td></tr></tbody></table>',1,'2018-11-12 08:56:03',NULL),(41,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/periods/edit-save/1','Update data  at Period','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2018-11-12 09:15:36',NULL),(42,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/periods/edit-save/1','Update data  at Period','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2018-11-12 09:19:28',NULL),(43,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/periods/edit-save/1','Update data  at Period','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2018-11-12 09:19:46',NULL),(44,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/periods/edit-save/1','Update data  at Period','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>',1,'2018-11-12 09:21:17',NULL),(45,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabelibarangs/add-save','Add New Data  at Nota Beli Barang','',1,'2018-11-13 20:19:13',NULL),(46,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/menu_management/add-save','Add New Data SI Master at Menu Management','',1,'2018-11-13 20:28:04',NULL),(47,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/menu_management/edit-save/11','Update data Sistem Informasi Beli at Menu Management','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Sistem Informasi</td><td>Sistem Informasi Beli</td></tr></tbody></table>',1,'2018-11-13 20:29:04',NULL),(48,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/menu_management/add-save','Add New Data Sistem Informasi Jual at Menu Management','',1,'2018-11-13 20:30:15',NULL),(49,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/sewapropertis/add-save','Add New Data Ruko at Sewa Properti','',1,'2018-11-27 06:32:29',NULL),(50,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabeliasets/add-save','Add New Data  at Nota Beli Aset','',1,'2018-11-27 06:36:25',NULL),(51,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabelibarangs/add-save','Add New Data  at Nota Beli Barang','',1,'2018-11-27 06:40:08',NULL),(52,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notajualbarangs/add-save','Add New Data  at Nota Jual Barang','',1,'2018-11-27 06:59:38',NULL),(53,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabelibarangs/add-save','Add New Data  at Nota Beli Barang','',1,'2018-11-27 07:06:16',NULL),(54,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notajualbarangs/add-save','Add New Data  at Nota Jual Barang','',1,'2018-11-27 07:10:07',NULL),(55,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/notabayarbiayas/add-save','Add New Data  at Nota Bayar Biaya','',1,'2018-11-27 07:12:22',NULL),(56,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/pelunasanbeli/add-save','Add New Data  at Pelunasan Pembelian','',1,'2018-11-27 07:16:01',NULL),(57,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/pelunasanbeli/add-save','Add New Data  at Pelunasan Pembelian','',1,'2018-11-27 07:19:05',NULL),(58,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/transactions/add-save','Add New Data  at Transaction','',1,'2018-11-27 07:25:40',NULL),(59,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/transactions/add-save','Add New Data  at Transaction','',1,'2018-11-27 07:27:18',NULL),(60,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36','http://localhost:8000/admin/transactions/edit-save/12','Update data  at Transaction','<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>reference_number</td><td></td><td></td></tr></tbody></table>',1,'2018-11-27 07:27:49',NULL);
/*!40000 ALTER TABLE `cms_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menus`
--

DROP TABLE IF EXISTS `cms_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menus`
--

LOCK TABLES `cms_menus` WRITE;
/*!40000 ALTER TABLE `cms_menus` DISABLE KEYS */;
INSERT INTO `cms_menus` VALUES (1,'Aset','Route','AdminAsetsControllerGetIndex',NULL,'fa fa-truck',17,1,0,1,4,'2018-11-10 02:47:37',NULL),(2,'Barang','Route','AdminBarangsControllerGetIndex',NULL,'fa fa-glass',17,1,0,1,1,'2018-11-10 02:49:03',NULL),(3,'Bank','Route','AdminBanksControllerGetIndex',NULL,'fa fa-building-o',17,1,0,1,2,'2018-11-10 02:49:59',NULL),(4,'Supplier','Route','AdminSuppliersControllerGetIndex',NULL,'fa fa-briefcase',17,1,0,1,3,'2018-11-10 02:51:46',NULL),(5,'Nota Beli Barang','Route','AdminNotabelibarangsControllerGetIndex',NULL,'fa fa-truck',11,1,0,1,1,'2018-11-10 02:53:46',NULL),(6,'Sewa Properti','Route','AdminSewapropertisControllerGetIndex',NULL,'fa fa-building',11,1,0,1,3,'2018-11-10 03:17:48',NULL),(7,'Nota Bayar Biaya','Route','AdminNotabayarbiayasControllerGetIndex',NULL,'fa fa-money',11,1,0,1,4,'2018-11-10 03:23:33',NULL),(8,'Nota Beli Aset','Route','AdminNotabeliasetsControllerGetIndex',NULL,'fa fa-home',11,1,0,1,2,'2018-11-10 03:27:13',NULL),(9,'Pelunasan Pembelian','Route','AdminPelunasanbeliControllerGetIndex',NULL,'fa fa-money',11,1,0,1,5,'2018-11-10 03:33:03',NULL),(10,'Nota Jual Barang','Route','AdminNotajualbarangsControllerGetIndex',NULL,'fa fa-shopping-cart',18,1,0,1,1,'2018-11-10 03:37:47',NULL),(11,'Sistem Informasi Beli','URL','http://kristian.ax.lt','green','fa fa-star-o',0,1,0,1,3,'2018-11-10 19:45:01','2018-11-13 20:29:04'),(12,'Akuntansi','URL','kristian.ax.lt','red','fa fa-money',0,1,0,1,4,'2018-11-10 19:47:32',NULL),(13,'Period','Route','AdminPeriodsControllerGetIndex',NULL,'fa fa-calendar-times-o',12,1,0,1,1,'2018-11-10 19:59:05',NULL),(14,'Account','Route','AdminAccountsControllerGetIndex',NULL,'fa fa-book',12,1,0,1,2,'2018-11-10 20:00:04',NULL),(16,'Transaction','Route','AdminTransactionsControllerGetIndex',NULL,'fa fa-money',12,1,0,1,3,'2018-11-10 20:03:00',NULL),(17,'SI Master','URL','kristian.ax.lt','normal','fa fa-square',0,1,0,1,1,'2018-11-13 20:28:04',NULL),(18,'Sistem Informasi Jual','URL','kristian.ax.lt','light-blue','fa fa-upload',0,1,0,1,2,'2018-11-13 20:30:15',NULL);
/*!40000 ALTER TABLE `cms_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menus_privileges`
--

DROP TABLE IF EXISTS `cms_menus_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menus_privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menus_privileges`
--

LOCK TABLES `cms_menus_privileges` WRITE;
/*!40000 ALTER TABLE `cms_menus_privileges` DISABLE KEYS */;
INSERT INTO `cms_menus_privileges` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,5,1),(6,6,1),(7,7,1),(8,8,1),(9,9,1),(10,10,1),(13,12,1),(14,13,1),(15,14,1),(16,15,1),(17,16,1),(18,17,1),(19,11,1),(20,18,1);
/*!40000 ALTER TABLE `cms_menus_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_moduls`
--

DROP TABLE IF EXISTS `cms_moduls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_moduls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_moduls`
--

LOCK TABLES `cms_moduls` WRITE;
/*!40000 ALTER TABLE `cms_moduls` DISABLE KEYS */;
INSERT INTO `cms_moduls` VALUES (1,'Notifications','fa fa-cog','notifications','cms_notifications','NotificationsController',1,1,'2018-11-10 02:42:54',NULL,NULL),(2,'Privileges','fa fa-cog','privileges','cms_privileges','PrivilegesController',1,1,'2018-11-10 02:42:54',NULL,NULL),(3,'Privileges Roles','fa fa-cog','privileges_roles','cms_privileges_roles','PrivilegesRolesController',1,1,'2018-11-10 02:42:54',NULL,NULL),(4,'Users Management','fa fa-users','users','cms_users','AdminCmsUsersController',0,1,'2018-11-10 02:42:54',NULL,NULL),(5,'Settings','fa fa-cog','settings','cms_settings','SettingsController',1,1,'2018-11-10 02:42:54',NULL,NULL),(6,'Module Generator','fa fa-database','module_generator','cms_moduls','ModulsController',1,1,'2018-11-10 02:42:54',NULL,NULL),(7,'Menu Management','fa fa-bars','menu_management','cms_menus','MenusController',1,1,'2018-11-10 02:42:54',NULL,NULL),(8,'Email Templates','fa fa-envelope-o','email_templates','cms_email_templates','EmailTemplatesController',1,1,'2018-11-10 02:42:54',NULL,NULL),(9,'Statistic Builder','fa fa-dashboard','statistic_builder','cms_statistics','StatisticBuilderController',1,1,'2018-11-10 02:42:54',NULL,NULL),(10,'API Generator','fa fa-cloud-download','api_generator','','ApiCustomController',1,1,'2018-11-10 02:42:54',NULL,NULL),(11,'Log User Access','fa fa-flag-o','logs','cms_logs','LogsController',1,1,'2018-11-10 02:42:54',NULL,NULL),(12,'Aset','fa fa-home','asets','asets','AdminAsetsController',0,0,'2018-11-10 02:47:37',NULL,NULL),(13,'Barang','fa fa-glass','barangs','barangs','AdminBarangsController',0,0,'2018-11-10 02:49:02',NULL,NULL),(14,'Bank','fa fa-building-o','banks','banks','AdminBanksController',0,0,'2018-11-10 02:49:58',NULL,NULL),(15,'Supplier','fa fa-briefcase','suppliers','suppliers','AdminSuppliersController',0,0,'2018-11-10 02:51:46',NULL,NULL),(16,'Nota Beli Barang','fa fa-truck','notabelibarangs','notabelibarangs','AdminNotabelibarangsController',0,0,'2018-11-10 02:53:45',NULL,NULL),(17,'Sewa Properti','fa fa-building','sewapropertis','sewapropertis','AdminSewapropertisController',0,0,'2018-11-10 03:17:48',NULL,NULL),(18,'Nota Bayar Biaya','fa fa-money','notabayarbiayas','notabayarbiayas','AdminNotabayarbiayasController',0,0,'2018-11-10 03:23:33',NULL,NULL),(19,'Nota Beli Aset','fa fa-home','notabeliasets','notabeliasets','AdminNotabeliasetsController',0,0,'2018-11-10 03:27:13',NULL,NULL),(20,'Pelunasan Pembelian','fa fa-money','pelunasanbeli','pelunasanbeli','AdminPelunasanbeliController',0,0,'2018-11-10 03:33:03',NULL,NULL),(21,'Nota Jual Barang','fa fa-shopping-cart','notajualbarangs','notajualbarangs','AdminNotajualbarangsController',0,0,'2018-11-10 03:37:46',NULL,NULL),(22,'Period','fa fa-calendar-times-o','periods','periods','AdminPeriodsController',0,0,'2018-11-10 19:59:05',NULL,NULL),(23,'Account','fa fa-book','accounts','accounts','AdminAccountsController',0,0,'2018-11-10 20:00:04',NULL,NULL),(24,'Transaction','fa fa-hand-grab-o','transactions','transactions','AdminTransactionsController',0,0,'2018-11-10 20:01:59',NULL,'2018-11-10 20:02:25'),(25,'Transaction','fa fa-money','transactions','transactions','AdminTransactionsController',0,0,'2018-11-10 20:03:00',NULL,NULL);
/*!40000 ALTER TABLE `cms_moduls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_notifications`
--

DROP TABLE IF EXISTS `cms_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_notifications`
--

LOCK TABLES `cms_notifications` WRITE;
/*!40000 ALTER TABLE `cms_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_privileges`
--

DROP TABLE IF EXISTS `cms_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_privileges`
--

LOCK TABLES `cms_privileges` WRITE;
/*!40000 ALTER TABLE `cms_privileges` DISABLE KEYS */;
INSERT INTO `cms_privileges` VALUES (1,'Super Administrator',1,'skin-red','2018-11-10 02:42:54',NULL);
/*!40000 ALTER TABLE `cms_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_privileges_roles`
--

DROP TABLE IF EXISTS `cms_privileges_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_privileges_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_privileges_roles`
--

LOCK TABLES `cms_privileges_roles` WRITE;
/*!40000 ALTER TABLE `cms_privileges_roles` DISABLE KEYS */;
INSERT INTO `cms_privileges_roles` VALUES (1,1,0,0,0,0,1,1,'2018-11-10 02:42:54',NULL),(2,1,1,1,1,1,1,2,'2018-11-10 02:42:54',NULL),(3,0,1,1,1,1,1,3,'2018-11-10 02:42:54',NULL),(4,1,1,1,1,1,1,4,'2018-11-10 02:42:54',NULL),(5,1,1,1,1,1,1,5,'2018-11-10 02:42:55',NULL),(6,1,1,1,1,1,1,6,'2018-11-10 02:42:55',NULL),(7,1,1,1,1,1,1,7,'2018-11-10 02:42:55',NULL),(8,1,1,1,1,1,1,8,'2018-11-10 02:42:55',NULL),(9,1,1,1,1,1,1,9,'2018-11-10 02:42:55',NULL),(10,1,1,1,1,1,1,10,'2018-11-10 02:42:55',NULL),(11,1,0,1,0,1,1,11,'2018-11-10 02:42:55',NULL),(12,1,1,1,1,1,1,12,NULL,NULL),(13,1,1,1,1,1,1,13,NULL,NULL),(14,1,1,1,1,1,1,14,NULL,NULL),(15,1,1,1,1,1,1,15,NULL,NULL),(16,1,1,1,1,1,1,16,NULL,NULL),(17,1,1,1,1,1,1,17,NULL,NULL),(18,1,1,1,1,1,1,18,NULL,NULL),(19,1,1,1,1,1,1,19,NULL,NULL),(20,1,1,1,1,1,1,20,NULL,NULL),(21,1,1,1,1,1,1,21,NULL,NULL),(22,1,1,1,1,1,1,22,NULL,NULL),(23,1,1,1,1,1,1,23,NULL,NULL),(24,1,1,1,1,1,1,24,NULL,NULL),(25,1,1,1,1,1,1,25,NULL,NULL);
/*!40000 ALTER TABLE `cms_privileges_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_settings`
--

DROP TABLE IF EXISTS `cms_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_settings`
--

LOCK TABLES `cms_settings` WRITE;
/*!40000 ALTER TABLE `cms_settings` DISABLE KEYS */;
INSERT INTO `cms_settings` VALUES (1,'login_background_color',NULL,'text',NULL,'Input hexacode','2018-11-10 02:42:55',NULL,'Login Register Style','Login Background Color'),(2,'login_font_color',NULL,'text',NULL,'Input hexacode','2018-11-10 02:42:55',NULL,'Login Register Style','Login Font Color'),(3,'login_background_image',NULL,'upload_image',NULL,NULL,'2018-11-10 02:42:55',NULL,'Login Register Style','Login Background Image'),(4,'email_sender','support@crudbooster.com','text',NULL,NULL,'2018-11-10 02:42:55',NULL,'Email Setting','Email Sender'),(5,'smtp_driver','mail','select','smtp,mail,sendmail',NULL,'2018-11-10 02:42:55',NULL,'Email Setting','Mail Driver'),(6,'smtp_host','','text',NULL,NULL,'2018-11-10 02:42:55',NULL,'Email Setting','SMTP Host'),(7,'smtp_port','25','text',NULL,'default 25','2018-11-10 02:42:55',NULL,'Email Setting','SMTP Port'),(8,'smtp_username','','text',NULL,NULL,'2018-11-10 02:42:55',NULL,'Email Setting','SMTP Username'),(9,'smtp_password','','text',NULL,NULL,'2018-11-10 02:42:55',NULL,'Email Setting','SMTP Password'),(10,'appname','CRUDBooster','text',NULL,NULL,'2018-11-10 02:42:55',NULL,'Application Setting','Application Name'),(11,'default_paper_size','Legal','text',NULL,'Paper size, ex : A4, Legal, etc','2018-11-10 02:42:55',NULL,'Application Setting','Default Paper Print Size'),(12,'logo','','upload_image',NULL,NULL,'2018-11-10 02:42:55',NULL,'Application Setting','Logo'),(13,'favicon','','upload_image',NULL,NULL,'2018-11-10 02:42:55',NULL,'Application Setting','Favicon'),(14,'api_debug_mode','true','select','true,false',NULL,'2018-11-10 02:42:55',NULL,'Application Setting','API Debug Mode'),(15,'google_api_key','','text',NULL,NULL,'2018-11-10 02:42:55',NULL,'Application Setting','Google API Key'),(16,'google_fcm_key','','text',NULL,NULL,'2018-11-10 02:42:55',NULL,'Application Setting','Google FCM Key');
/*!40000 ALTER TABLE `cms_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_statistic_components`
--

DROP TABLE IF EXISTS `cms_statistic_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_statistic_components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_statistic_components`
--

LOCK TABLES `cms_statistic_components` WRITE;
/*!40000 ALTER TABLE `cms_statistic_components` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_statistic_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_statistics`
--

DROP TABLE IF EXISTS `cms_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_statistics`
--

LOCK TABLES `cms_statistics` WRITE;
/*!40000 ALTER TABLE `cms_statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_users`
--

DROP TABLE IF EXISTS `cms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_users`
--

LOCK TABLES `cms_users` WRITE;
/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
INSERT INTO `cms_users` VALUES (1,'Super Admin',NULL,'admin@crudbooster.com','$2y$10$Yr2OQdViBFO2hivC1sG7ge/XXjifDDBqAjsK2D1PDFkdgDuTliNVi',1,'2018-11-10 02:42:54',NULL,'Active');
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2016_08_07_145904_add_table_cms_apicustom',1),(2,'2016_08_07_150834_add_table_cms_dashboard',1),(3,'2016_08_07_151210_add_table_cms_logs',1),(4,'2016_08_07_151211_add_details_cms_logs',1),(5,'2016_08_07_152014_add_table_cms_privileges',1),(6,'2016_08_07_152214_add_table_cms_privileges_roles',1),(7,'2016_08_07_152320_add_table_cms_settings',1),(8,'2016_08_07_152421_add_table_cms_users',1),(9,'2016_08_07_154624_add_table_cms_menus_privileges',1),(10,'2016_08_07_154624_add_table_cms_moduls',1),(11,'2016_08_17_225409_add_status_cms_users',1),(12,'2016_08_20_125418_add_table_cms_notifications',1),(13,'2016_09_04_033706_add_table_cms_email_queues',1),(14,'2016_09_16_035347_add_group_setting',1),(15,'2016_09_16_045425_add_label_setting',1),(16,'2016_09_17_104728_create_nullable_cms_apicustom',1),(17,'2016_10_01_141740_add_method_type_apicustom',1),(18,'2016_10_01_141846_add_parameters_apicustom',1),(19,'2016_10_01_141934_add_responses_apicustom',1),(20,'2016_10_01_144826_add_table_apikey',1),(21,'2016_11_14_141657_create_cms_menus',1),(22,'2016_11_15_132350_create_cms_email_templates',1),(23,'2016_11_15_190410_create_cms_statistics',1),(24,'2016_11_17_102740_create_cms_statistic_components',1),(25,'2017_06_06_164501_add_deleted_at_cms_moduls',1),(31,'2018_10_06_140411_create_periods_table',2),(32,'2018_10_06_140423_create_accounts_table',2),(33,'2018_10_06_140429_create_period_balances_table',2),(34,'2018_10_06_140444_create_transactions_table',2),(35,'2018_10_06_140450_create_transaction_details_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notabayarbiayas`
--

DROP TABLE IF EXISTS `notabayarbiayas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notabayarbiayas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `keterangan` text,
  `nominal` double DEFAULT NULL,
  `cara_bayar` varchar(191) DEFAULT NULL,
  `nomor_rekening` varchar(191) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `account_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notabayarbiaya_banks1_idx` (`bank_id`),
  CONSTRAINT `fk_notabayarbiaya_banks1` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notabayarbiayas`
--

LOCK TABLES `notabayarbiayas` WRITE;
/*!40000 ALTER TABLE `notabayarbiayas` DISABLE KEYS */;
INSERT INTO `notabayarbiayas` VALUES (1,'2018-07-10 00:00:00','Bayar Listrik',250000,'Tunai Transfer','111222444',1,509);
/*!40000 ALTER TABLE `notabayarbiayas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notabeliasets`
--

DROP TABLE IF EXISTS `notabeliasets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notabeliasets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `aset_id` int(11) NOT NULL,
  `harga_beli` double DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `cara_bayar` varchar(191) DEFAULT NULL,
  `uang_muka_cicilan` double DEFAULT NULL,
  `durasi_cicilan` varchar(191) DEFAULT NULL,
  `nomor_rekening` varchar(191) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `fob` varchar(191) DEFAULT NULL,
  `biaya_kirim` double DEFAULT NULL,
  `resi_kirim` varchar(191) DEFAULT NULL,
  `is_lunas` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notabeliasets_suppliers1_idx` (`supplier_id`),
  KEY `fk_notabeliasets_banks1_idx` (`bank_id`),
  KEY `fk_notabeliasets_asets1_idx` (`aset_id`),
  CONSTRAINT `fk_notabeliasets_asets1` FOREIGN KEY (`aset_id`) REFERENCES `asets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_notabeliasets_banks1` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_notabeliasets_suppliers1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notabeliasets`
--

LOCK TABLES `notabeliasets` WRITE;
/*!40000 ALTER TABLE `notabeliasets` DISABLE KEYS */;
INSERT INTO `notabeliasets` VALUES (1,'2018-07-01 00:00:00',3,1,12000000,1,'Cicilan',1000000,'11',NULL,NULL,'FOB Shipping Point',NULL,NULL,NULL);
/*!40000 ALTER TABLE `notabeliasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notabelibarangs`
--

DROP TABLE IF EXISTS `notabelibarangs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notabelibarangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `fob` varchar(191) DEFAULT NULL,
  `biaya_kirim` double DEFAULT NULL,
  `resi_kirim` varchar(191) DEFAULT NULL,
  `cara_bayar` varchar(191) DEFAULT NULL,
  `discount_term` varchar(191) DEFAULT NULL,
  `nomor_rekening` varchar(191) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `is_lunas` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notabelibarang_supplier_idx` (`supplier_id`),
  KEY `fk_notabelibarang_bank1_idx` (`bank_id`),
  CONSTRAINT `fk_notabelibarang_bank1` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_notabelibarang_supplier` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notabelibarangs`
--

LOCK TABLES `notabelibarangs` WRITE;
/*!40000 ALTER TABLE `notabelibarangs` DISABLE KEYS */;
INSERT INTO `notabelibarangs` VALUES (2,'2018-07-02 00:00:00',2,'FOB Destination',NULL,NULL,'Kredit','2/15 n/EOM',NULL,NULL,1),(3,'2018-07-03 00:00:00',1,'FOB Shipping Point',150000,NULL,'Tunai Transfer',NULL,'111222111',2,NULL);
/*!40000 ALTER TABLE `notabelibarangs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notabelibarangs_barangs`
--

DROP TABLE IF EXISTS `notabelibarangs_barangs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notabelibarangs_barangs` (
  `notabelibarang_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`notabelibarang_id`,`barang_id`),
  KEY `fk_notabelibarangs_has_barangs_barangs1_idx` (`barang_id`),
  KEY `fk_notabelibarangs_has_barangs_notabelibarangs1_idx` (`notabelibarang_id`),
  CONSTRAINT `fk_notabelibarangs_has_barangs_barangs1` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_notabelibarangs_has_barangs_notabelibarangs1` FOREIGN KEY (`notabelibarang_id`) REFERENCES `notabelibarangs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notabelibarangs_barangs`
--

LOCK TABLES `notabelibarangs_barangs` WRITE;
/*!40000 ALTER TABLE `notabelibarangs_barangs` DISABLE KEYS */;
INSERT INTO `notabelibarangs_barangs` VALUES (2,1,100,15000,NULL),(2,2,200,5000,NULL),(3,3,50,40000,NULL),(3,4,100,15000,NULL);
/*!40000 ALTER TABLE `notabelibarangs_barangs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notajualbarangs`
--

DROP TABLE IF EXISTS `notajualbarangs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notajualbarangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `status` varchar(191) DEFAULT NULL COMMENT '"MASUK" = user sudah checkout dan pilih cara bayar, tetapi belum bayar\n"TERBAYAR" = pembayaran sudah diterima sistem, karyawan belum melihat pesanan ini\n"PACKING" = karyawan sudah melihat pesanan ini, dan sedang mengerjakan pengambilan,persiapan,packing barang\n"DIKIRIM" = karyawan sudah mengirim barang lewat salah satu jasa ekspedisi, tetapi belum sampai di tujuan\n"SELESAI" = barang sudah diterima pembeli',
  `fob` varchar(191) DEFAULT NULL,
  `biaya_kirim` double DEFAULT NULL,
  `resi_kirim` varchar(191) DEFAULT NULL,
  `cara_bayar` varchar(191) DEFAULT NULL,
  `nomor_kartu_kredit` varchar(191) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `discount_term` varchar(191) DEFAULT NULL,
  `nomor_rekening` varchar(191) DEFAULT NULL,
  `is_lunas` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notajualbarangs_banks1_idx` (`bank_id`),
  CONSTRAINT `fk_notajualbarangs_banks1` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notajualbarangs`
--

LOCK TABLES `notajualbarangs` WRITE;
/*!40000 ALTER TABLE `notajualbarangs` DISABLE KEYS */;
INSERT INTO `notajualbarangs` VALUES (1,'2018-07-03 00:00:00',NULL,'FOB Shipping Point',NULL,NULL,'Kartu Kredit','00997788',1,NULL,'',NULL),(2,'2018-07-05 00:00:00',NULL,'FOB Shipping Point',NULL,NULL,'Tunai Transfer',NULL,2,NULL,'000999000999',NULL);
/*!40000 ALTER TABLE `notajualbarangs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notajualbarangs_barangs`
--

DROP TABLE IF EXISTS `notajualbarangs_barangs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notajualbarangs_barangs` (
  `notajualbarang_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`notajualbarang_id`,`barang_id`),
  KEY `fk_notajualbarangs_has_barangs_barangs1_idx` (`barang_id`),
  KEY `fk_notajualbarangs_has_barangs_notajualbarangs1_idx` (`notajualbarang_id`),
  CONSTRAINT `fk_notajualbarangs_has_barangs_barangs1` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_notajualbarangs_has_barangs_notajualbarangs1` FOREIGN KEY (`notajualbarang_id`) REFERENCES `notajualbarangs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notajualbarangs_barangs`
--

LOCK TABLES `notajualbarangs_barangs` WRITE;
/*!40000 ALTER TABLE `notajualbarangs_barangs` DISABLE KEYS */;
INSERT INTO `notajualbarangs_barangs` VALUES (1,2,50,20000,0,NULL),(2,1,10,75000,7500,NULL),(2,3,5,100000,10000,NULL),(2,4,10,50000,5000,NULL);
/*!40000 ALTER TABLE `notajualbarangs_barangs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelunasanbelis`
--

DROP TABLE IF EXISTS `pelunasanbelis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pelunasanbelis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `notabelibarang_id` int(11) DEFAULT NULL,
  `notabeliaset_id` int(11) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `cara_bayar` varchar(191) DEFAULT NULL,
  `biaya_transfer` double DEFAULT NULL,
  `nomor_rekening` varchar(191) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pelunasanbelibarangs_notabelibarangs1_idx` (`notabelibarang_id`),
  KEY `fk_pelunasanbeli_notabeliasets1_idx` (`notabeliaset_id`),
  KEY `fk_pelunasanbelis_banks1_idx` (`bank_id`),
  CONSTRAINT `fk_pelunasanbeli_notabeliasets1` FOREIGN KEY (`notabeliaset_id`) REFERENCES `notabeliasets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pelunasanbelibarangs_notabelibarangs1` FOREIGN KEY (`notabelibarang_id`) REFERENCES `notabelibarangs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pelunasanbelis_banks1` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelunasanbelis`
--

LOCK TABLES `pelunasanbelis` WRITE;
/*!40000 ALTER TABLE `pelunasanbelis` DISABLE KEYS */;
INSERT INTO `pelunasanbelis` VALUES (1,'2018-07-15 00:00:00',2,NULL,2450000,'Tunai Transfer',10000,'444666777444',1),(2,'2018-07-30 00:00:00',NULL,1,1000000,'Tunai Transfer',0,'1991',1);
/*!40000 ALTER TABLE `pelunasanbelis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `period_balances`
--

DROP TABLE IF EXISTS `period_balances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `period_balances` (
  `period_id` int(10) unsigned NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `opening_balance` double DEFAULT NULL,
  `closing_balance` double DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`period_id`,`account_id`),
  KEY `period_balances_account_id_foreign` (`account_id`),
  CONSTRAINT `period_balances_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `period_balances_period_id_foreign` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `period_balances`
--

LOCK TABLES `period_balances` WRITE;
/*!40000 ALTER TABLE `period_balances` DISABLE KEYS */;
INSERT INTO `period_balances` VALUES (1,101,10000000,9000000,NULL),(1,102,30000000,15290000,NULL),(1,103,20000000,17925000,NULL),(1,104,5000000,5000000,NULL),(1,105,30000000,30000000,NULL),(1,106,NULL,11000000,NULL),(1,107,NULL,5335000,NULL),(1,108,NULL,0,NULL),(1,110,NULL,12000000,NULL),(1,111,NULL,500000,NULL),(1,201,15000000,15000000,NULL),(1,202,30000000,40000000,NULL),(1,301,50000000,50050000,NULL),(1,302,NULL,0,NULL),(1,401,NULL,0,NULL),(1,402,NULL,0,NULL),(1,405,NULL,0,NULL),(1,501,NULL,0,NULL),(1,506,NULL,0,NULL),(1,507,NULL,0,NULL),(1,508,NULL,0,NULL),(1,509,NULL,0,NULL),(1,515,NULL,0,NULL),(1,520,NULL,0,NULL),(1,901,NULL,0,NULL);
/*!40000 ALTER TABLE `period_balances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periods`
--

DROP TABLE IF EXISTS `periods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_closed` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periods`
--

LOCK TABLES `periods` WRITE;
/*!40000 ALTER TABLE `periods` DISABLE KEYS */;
INSERT INTO `periods` VALUES (1,'2018-07-01 00:00:00','2018-07-31 20:00:00','Period yang diujikan di pdf',1,1);
/*!40000 ALTER TABLE `periods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piutangkartukredits`
--

DROP TABLE IF EXISTS `piutangkartukredits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piutangkartukredits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `keterangan` text,
  `bank_id` int(11) NOT NULL,
  `nomor_kartu_kredit` varchar(191) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `is_sudah_ditarik` tinyint(4) DEFAULT NULL,
  `ditarik_tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_piutangkartukredits_banks1_idx` (`bank_id`),
  CONSTRAINT `fk_piutangkartukredits_banks1` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piutangkartukredits`
--

LOCK TABLES `piutangkartukredits` WRITE;
/*!40000 ALTER TABLE `piutangkartukredits` DISABLE KEYS */;
INSERT INTO `piutangkartukredits` VALUES (1,'2018-07-03 00:00:00','JUAL-1',1,'00997788',1000000,1,'2018-07-30 14:20:40');
/*!40000 ALTER TABLE `piutangkartukredits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sewapropertis`
--

DROP TABLE IF EXISTS `sewapropertis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sewapropertis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `nama` varchar(191) DEFAULT NULL,
  `alamat` text,
  `biaya` double DEFAULT NULL,
  `durasi_sewa_bulan` int(11) DEFAULT NULL,
  `cara_bayar` varchar(191) DEFAULT NULL,
  `nomor_rekening` varchar(191) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sewapropertis_banks1_idx` (`bank_id`),
  CONSTRAINT `fk_sewapropertis_banks1` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sewapropertis`
--

LOCK TABLES `sewapropertis` WRITE;
/*!40000 ALTER TABLE `sewapropertis` DISABLE KEYS */;
INSERT INTO `sewapropertis` VALUES (1,'2018-07-01 00:00:00','Ruko','Jalan Raya Kalirungkut',12000000,12,'Tunai Transfer','00990099',1);
/*!40000 ALTER TABLE `sewapropertis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'UKM Kreatifitas Alam'),(2,'UKM Sejahtera Crafting'),(3,'Other');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_details`
--

DROP TABLE IF EXISTS `transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_details` (
  `transaction_id` int(10) unsigned NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `taccount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`,`account_id`),
  KEY `transaction_details_account_id_foreign` (`account_id`),
  CONSTRAINT `transaction_details_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaction_details_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_details`
--

LOCK TABLES `transaction_details` WRITE;
/*!40000 ALTER TABLE `transaction_details` DISABLE KEYS */;
INSERT INTO `transaction_details` VALUES (2,102,'CREDIT',12000000,NULL),(2,106,'DEBIT',12000000,NULL),(3,101,'CREDIT',1000000,NULL),(3,110,'DEBIT',12000000,NULL),(3,202,'CREDIT',11000000,NULL),(4,107,'DEBIT',2500000,NULL),(4,201,'CREDIT',2500000,NULL),(5,105,'DEBIT',1000000,NULL),(5,107,'CREDIT',250000,NULL),(5,401,'CREDIT',1000000,NULL),(5,501,'DEBIT',250000,NULL),(6,103,'CREDIT',3650000,NULL),(6,107,'DEBIT',3650000,NULL),(7,103,'DEBIT',1575000,NULL),(7,107,'CREDIT',515000,NULL),(7,401,'CREDIT',1750000,NULL),(7,402,'DEBIT',175000,NULL),(7,501,'DEBIT',515000,NULL),(8,102,'CREDIT',250000,NULL),(8,509,'DEBIT',250000,NULL),(9,102,'CREDIT',2460000,NULL),(9,107,'CREDIT',50000,NULL),(9,201,'DEBIT',2500000,NULL),(9,520,'DEBIT',10000,NULL),(10,102,'CREDIT',1000000,NULL),(10,202,'DEBIT',1000000,NULL),(11,102,'DEBIT',1000000,NULL),(11,105,'CREDIT',1000000,NULL),(12,111,'CREDIT',500000,NULL),(12,508,'DEBIT',500000,NULL),(13,106,'CREDIT',1000000,NULL),(13,507,'DEBIT',1000000,NULL),(14,401,'DEBIT',2750000,NULL),(14,402,'CREDIT',175000,NULL),(14,405,'DEBIT',0,NULL),(14,901,'CREDIT',2575000,NULL),(15,501,'CREDIT',765000,NULL),(15,506,'CREDIT',0,NULL),(15,507,'CREDIT',1000000,NULL),(15,508,'CREDIT',500000,NULL),(15,509,'CREDIT',250000,NULL),(15,515,'CREDIT',0,NULL),(15,520,'CREDIT',10000,NULL),(15,901,'DEBIT',2525000,NULL),(16,301,'CREDIT',50000,NULL),(16,901,'DEBIT',50000,NULL),(17,301,'DEBIT',0,NULL),(17,302,'CREDIT',0,NULL);
/*!40000 ALTER TABLE `transaction_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `registered_at` datetime DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (2,'2018-07-01 00:00:00','SEWA-1',NULL,'NORMAL'),(3,'2018-07-01 00:00:00','BLIA-1',NULL,'NORMAL'),(4,'2018-07-02 00:00:00','BELI-2',NULL,'NORMAL'),(5,'2018-07-03 00:00:00','JUAL-1',NULL,'NORMAL'),(6,'2018-07-03 00:00:00','BELI-3',NULL,'NORMAL'),(7,'2018-07-05 00:00:00','JUAL-2',NULL,'NORMAL'),(8,'2018-07-10 00:00:00','BBYA-1',NULL,'NORMAL'),(9,'2018-07-15 00:00:00','LBRG-1',NULL,'NORMAL'),(10,'2018-07-30 00:00:00','LASE-2',NULL,'NORMAL'),(11,'2018-07-30 14:20:40','TPKK-1',NULL,'NORMAL'),(12,'2018-07-31 00:00:00','Penyesuaian motor, diinput manual',NULL,'ADJUSTMENT'),(13,'2018-07-31 00:00:00','Penyesuaian sewa muka',NULL,'ADJUSTMENT'),(14,'2018-07-31 20:00:00','Zero-ing INCOMES',NULL,'CLOSING'),(15,'2018-07-31 20:00:00','Zero-ing EXPENSES',NULL,'CLOSING'),(16,'2018-07-31 20:00:00','Zero-ing SUMMARY',NULL,'CLOSING'),(17,'2018-07-31 20:00:00','Zero-ing EQUITY',NULL,'CLOSING');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-27 21:55:29
