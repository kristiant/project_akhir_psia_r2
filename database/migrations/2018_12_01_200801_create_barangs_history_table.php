<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangs_history', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('barang_id');
            $table->foreign('barang_id')->references('id')->on('barangs');
            $table->dateTime('tanggal')->nullable();
            $table->integer('jumlah')->signed()->nullable();
            $table->double('harga')->nullable();
            $table->integer('saldo_jumlah')->nullable();
            $table->double('saldo_harga')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangs_history');
    }
}
