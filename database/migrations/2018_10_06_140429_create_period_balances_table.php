<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeriodBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('period_balances', function (Blueprint $table) {
            /*
            $table->increments('id');
            $table->timestamps();
            $table->integer('period_id')->unsigned();
            $table->integer('account_id')->unsigned();
            $table->double('opening_balance')->nullable();
            $table->double('closing_balance')->nullable();
            $table->foreign('period_id')->references('id')->on('periods')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade')->onUpdate('cascade');
            */

            $table->integer('period_id')->unsigned();//->primary();
            $table->integer('account_id')->unsigned();//->primary();
            $table->double('opening_balance')->nullable();
            $table->double('closing_balance')->nullable();
            $table->integer('id')->nullable();
            $table->foreign('period_id')->references('id')->on('periods')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['period_id','account_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('period_balances');
    }
}
