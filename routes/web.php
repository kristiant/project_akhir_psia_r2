<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

// TUTUP PERIODE
Route::get('/admin/period/close', 'PeriodController@showPeriod')->name('ClosePeriod.showPeriod');
Route::post('/admin/period/close', 'PeriodController@closePeriod')->name('ClosePeriod.post.close');

// TARIK PIUTANG KARTU KREDIT
Route::get('/admin/draw/cc', 'DrawCreditController@showBank')->name('DrawCredit.showBank');
Route::post('/admin/draw/cc', 'DrawCreditController@chooseBank')->name('DrawCredit.post.chooseBank');
Route::get('/admin/draw/cc/{bank_id}', 'DrawCreditController@showCredit')->name('DrawCredit.showCredit');
Route::post('/admin/draw/cc/withdraw', 'DrawCreditController@withdraw')->name('DrawCredit.post.withdraw');

// LAPORAN
Route::get('/admin/report/index', 'ReportController@showIndex')->name('Report.showIndex');
Route::post('/admin/report/index/{report_type}/{period_id}', 'ReportController@chooseReport')->name('Report.chooseReport');
Route::get('/admin/report/journal/{period_id}', 'ReportController@showJournal')->name('Report.showJournal');
Route::get('/admin/report/ledger/{period_id}', 'ReportController@showLedger')->name('Report.showLedger');
Route::get('/admin/report/worksheet/{period_id}', 'ReportController@showWorksheet')->name('Report.showWorksheet');
Route::get('/admin/report/reports/{period_id}', 'ReportController@showReports')->name('Report.showReports');
Route::get('/admin/report/stock/{product_id}', 'ReportController@showStock')->name('Report.showStock');

// AJAX
// .. PELUNASAN BELI
Route::get('/ajax/pelunasanbeli/sisahutang/barang/{nota_beli_barang_id}/{tanggal}', 'AjaxController@sisaHutangBarang')->name('AjaxController.sisaHutangBarang');
Route::get('/ajax/pelunasanbeli/sisahutang/aset/{nota_beli_aset_id}/{tanggal}', 'AjaxController@sisaHutangAset')->name('AjaxController.sisaHutangAset');

// UNUSED
// ..TARIKPIUTANGKARTUKREDIT
// Route::get('/form/tarikpiutangkartukredit', 'TarikPiutangKartuKredit@showFormPilihBank')->name('TarikPiutangKartuKredit.showFormPilihBank');
// Route::post('/admin/tarikpiutangkartukredit', 'TarikPiutangKartuKredit@pilihBank')->name('TarikPiutangKartuKredit.pilihBank');
// Route::get('/admin/tarikpiutangkartukredit/{bank_id}', 'TarikPiutangKartuKredit@showFormTarik')->name('TarikPiutangKartuKredit.showFormTarik');
// Route::post('/admin/tarikpiutangkartukredittt/proses', 'TarikPiutangKartuKredit@proses')->name('TarikPiutangKartuKredit.proses');

// ..SAMPLES TO HELP COLLABORATORS UNDERSTAND THIS PROJECT (DON'T USE ON PRESENTATION/PRODUCTION)
Route::get('samples/journal/{period_id}', 'SamplePagesController@journal')->name('sample_journal');
Route::get('samples/ledger/{period_id}', 'SamplePagesController@ledger');
Route::get('samples/worksheet/{period_id}', 'SamplePagesController@worksheet');
Route::get('samples/report/{period_id}', 'SamplePagesController@report');

Route::get('/testing', 'SamplePagesController@testing');
Route::get('/ajax/pelunasanbeli/sisahutang/barang/{nota_beli_barang_id}', 'AjaxController@sisaHutangBarang')->name('AjaxController.sisaHutangBarang');
Route::get('/ajax/pelunasanbeli/sisahutang/aset/{nota_beli_aset_id}', 'AjaxController@sisaHutangAset')->name('AjaxController.sisaHutangAset');