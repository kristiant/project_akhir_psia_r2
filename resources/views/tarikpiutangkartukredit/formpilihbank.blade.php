@section('pageTitle')
    Tarik Piutang Kartu Kredit
@endsection

@extends('master')

@section('content')
    @parent

    <div class="card w-50 mx-auto" style="margin-top: 10%;">
        <div class="card-header">
            <h5 class="card-title text-center">Pilih Bank</h5>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('drawCC.chooseBank') }}">
            @csrf
                <div class="form-group">
                    <select class="form-control col-8 mx-auto" name="bank_id">
                        <option selected>-- Pilih Bank --</option>
                        @foreach($banks as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="form-control btn btn-primary col-3" value="submit">Submit</button>
                </div>
        </form>
      </div>
    </div>
@endsection