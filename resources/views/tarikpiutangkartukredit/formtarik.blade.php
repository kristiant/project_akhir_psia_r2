@section('pageTitle')
    Tarik Piutang Kartu Kredit
@endsection

@extends('master')

@section('content')
    @parent

    <div class="card w-80 mx-auto" style="margin-top: 10%;">
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h5>Bank {{ $bank->nama }}</h5>
                </div>
                <div class="col-6 text-right">
                    <h5 class="card-title clock" style="display: inline;"></h5>
                </div>
            </div>
        </div>
        <div class="card-body">

            <form method="POST" action="{{ route('TarikPiutangKartuKredit.proses') }}">
                @csrf
                <div class="form-group">
                    <label>Tanggal Tarik</label>
                    <input class="form-control col-3 form-inline" type="text" name="tanggal_tarik" value="{{ date('Y-m-d H:i:s') }}" placeholder="Tanggal Tarik">
                </div>
                <div class="form-group">
                    <input type="hidden" name="bank_id" value="{{ $bank->id }}">
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="bg-dark text-white text-center" colspan="5">Daftar Transaksi</th>
                        </tr>
                        <tr>
                            <th>ID</th>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                            <th>Nomor Kartu Kredit</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($arrPiutang as $item)
                        <tr>
                            <td>
                                <input type="hidden" name="arr_piutangkartukredit_id[]" value="{{ $item->id }}">{{ $item->id }}
                            </td>
                            <td>{{ $item->tanggal }}</td>
                            <td>{{ $item->keterangan }}</td>
                            <td>{{ $item->nomor_kartu_kredit }}</td>
                            <td>{{ $item->nominal }}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                <div class="form-group text-center">
                    <a href="{{ url('/form/tarikpiutangkartukredit') }}" class="form-control btn btn-danger text-white col-3">
                    Kembali</a>
                    <button type="submit" class="form-control btn btn-success col-3"
                        @if(count($arrPiutang) == 0) disabled @endif>
                    Tarik Semua Transaksi</button>
                </div>
            </form>

        </div>
    </div>

    <script type="text/javascript">

        setInterval(function()
        {
            var date = new Date();
            var timeString =
                addZero(date.getHours())+":"+
                addZero(date.getMinutes())+":"+
                addZero(date.getSeconds());
            var dateString =
                addZero(date.getDate())+"-"+
                addZero(date.getMonth())+"-"+
                addZero(date.getFullYear());

            $('.clock').html(dateString+" "+timeString);
            // $('input[name="tanggal_tarik"]').val(reverseDateFormat(date)+" "+timeString);
        },
        1000);

        function addZero(i)
        {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        function reverseDateFormat(date)
        {
            var reversed =
                addZero(date.getFullYear())+"-"+
                addZero(date.getMonth())+"-"+
                addZero(date.getDate());
            return reversed;
        }
    </script>
@endsection