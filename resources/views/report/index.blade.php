@section('pageTitle')
    Pilih Laporan
@endsection

@extends('master')

@section('content')
    @parent

    <div class="card w-50 mx-auto" style="margin-top: 10%;">
        <div class="card-header">
            <h5 class="card-title text-center">Laporan</h5>
        </div>
        <div class="card-body">
            <form method="POST" action="#">
            @csrf
                <div class="form-group">
                    <select class="form-control col-8 mx-auto" name="report_type" id="report_type">
                        <option value="none" selected>-- Pilih Tipe Laporan --</option>
                        <option value="journal">Jurnal</option>
                        <option value="ledger">Buku Besar</option>
                        <option value="worksheet">Worksheet</option>
                        <option value="reports">Laporan</option>
                        <option value="stock">Kartu Stok</option>
                    </select>
                </div>
                <div class="form-group" id="div_choose_period">
                    <select class="form-control col-8 mx-auto" name="period_id" id="period_id">
                        <option value="none" selected>-- Pilih Periode --</option>
                        @foreach($periods as $item)
                            <option value="{{ $item->id }}">
                                {{ $item->id }} - {{ $item->comment }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group"  id="div_choose_product">
                    <select class="form-control col-8 mx-auto" name="product_id" id="product_id">
                        <option value="none" selected>-- Pilih Barang --</option>
                        @foreach($products as $item)
                            <option value="{{ $item->id }}">
                                {{ $item->id }} - {{ $item->nama }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group text-center">
                    <button id="submitForm" type="submit" class="form-control btn btn-primary col-3" value="submit">Show</button>
                </div>
        </form>
      </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() 
        {
            $('#submitForm').prop('disabled', true);

            $('#report_type').val("none");

            $('#period_id').val("none");
            $('#product_id').val("none");

            $('#div_choose_period').hide();
            $('#div_choose_product').hide();
        });

        $('#report_type').on('change', function() 
        {
            if($('#report_type').val() != "none" && 
              ($('#period_id').val() != "none" || $('#product_id').val() != "none"))
            {
                $('#submitForm').prop('disabled', false);
            }
            else
            {
                $('#submitForm').prop('disabled', true);
            }

            if($('#report_type').val() == "none")
            {
                $('#period_id').val("none");
                $('#product_id').val("none");

                $('#div_choose_period').hide();
                $('#div_choose_product').hide();

                $('#submitForm').prop('disabled', true);
            }
            else if($('#report_type').val() == "stock")
            {
                $('#period_id').val("none");
                $('#product_id').val("none");

                $('#div_choose_period').hide();
                $('#div_choose_product').show();

                $('#submitForm').prop('disabled', true);
            }
            else if($('#report_type').val() != "stock")
            {
                $('#period_id').val("none");
                $('#product_id').val("none");

                $('#div_choose_period').show();
                $('#div_choose_product').hide();

                $('#submitForm').prop('disabled', true);
            }
        });

        $('#period_id').on('change', function() 
        {
            if($('#report_type').val() != "none" && $('#period_id').val() != "none")
            {
                $('#submitForm').prop('disabled', false);
            }
            else
            {
                $('#submitForm').prop('disabled', true);
            }
        });

        $('#product_id').on('change', function() 
        {
            if($('#report_type').val() != "none" && $('#product_id').val() != "none")
            {
                $('#submitForm').prop('disabled', false);
            }
            else
            {
                $('#submitForm').prop('disabled', true);
            }
        });

        $("#submitForm").click(function(e){
            e.preventDefault();
            var rt = $("#report_type").val();
            var pid = -1;

            if(rt != 'stock')
            {
                pid = $("#period_id").val();   
            }
            else
            {
                pid = $("#product_id").val();
            }
            
            window.location.href = "/admin/report/"+rt+"/"+pid;
        });
    </script>
@endsection