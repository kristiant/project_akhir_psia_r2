@section('pageTitle')
    Laporan
@endsection

@extends('master')

@section('content')
	@parent

	<div class="container">
		
		<h1>Laporan keuangan (report) untuk periode: {{ $period->start }} sampai {{ $period->end }} ( {{ $period->comment }} )</h1>

		<div class="w-50 mx-auto mt-5">
			<h2 class="text-center">LAPORAN LABA RUGI</h2>
			<table class="table table-bordered">
				<thead class="thead-dark">
					<tr>
						<th colspan="2">INCOME</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $report["IncomeStatement"]["Incomes"] as $income )
						<tr>
							<td>{{ $income["Account"]->name }}</td>
							<td>{{ number_format((-1 * $income["Value"])) }}</td>
						</tr>
					@endforeach
					<tr>
						<th>Total Income:</th>
						<th>{{ number_format((-1 * $report["IncomeStatement"]["Totals"]["Incomes"])) }}</th>
					</tr>
				</tbody>
				<thead class="thead-dark">
					<tr>
						<th colspan="2">EXPENSE</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $report["IncomeStatement"]["Expenses"] as $expense )
						<tr>
							<td>{{ $expense["Account"]->name }}</td>
							<td>{{ number_format($expense["Value"]) }}</td>
						</tr>
					@endforeach
					<tr>
						<th>Total Expense:</th>
						<th>{{ number_format($report["IncomeStatement"]["Totals"]["Expenses"]) }}</th>
					</tr>

				<thead class="thead-dark">
					<tr>
						<th colspan="2">KESIMPULAN</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>LABA/RUGI:</th>
						<th>{{ number_format((-1 * ($report["IncomeStatement"]["Totals"]["Incomes"] + $report["IncomeStatement"]["Totals"]["Expenses"]))) }}</th>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="w-50 mx-auto mt-5">
			<h2 class="text-center">LAPORAN PERUBAHAN EKUITAS</h2>
			<table class="table table-bordered">
				<thead class="thead-dark">
					<tr>
						<th colspan="2">EQUITY</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Modal Awal</td>
						<td>{{ number_format($report["EquityStatement"]["OpeningEquity"]) }}</td>
					</tr>
					<tr>
						<td>Laba/Rugi</td>
						<td>{{ number_format($report["EquityStatement"]["Income/Expense"]) }}</td>
					</tr>
					<tr>
						<td>Prive</td>
						<td>{{ number_format($report["EquityStatement"]["Prive"]) }}</td>
					</tr>
					<tr>
						<th>Modal Akhir</th>
						<th>
							{{ number_format((
								$report["EquityStatement"]["OpeningEquity"]
								+ $report["EquityStatement"]["Income/Expense"]
								- $report["EquityStatement"]["Prive"]
							)) }}
						</th>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="w-50 mx-auto mt-5">
			<h2 class="text-center">LAPORAN NERACA</h2>
			<table class="table table-bordered mx-auto">
				<thead class="thead-dark">
					<tr>
						<th colspan="4">AKTIVA</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $report["BalanceSheet"]["Assets"]["Normal"] as $asset )
						<tr>
							<td><span></span></td>
							<td>{{ $asset["Account"]->name }}</td>
							<td>{{ number_format($asset["Value"]) }}</td>
							<td><span></span></td>
						</tr>
					@endforeach
					@foreach( $report["BalanceSheet"]["Assets"]["Depreciating"] as $asset )
						<tr>
							<td colspan="4"><span></span></td>
						</tr>
						<tr>
							<td><span></span></td>
							<td>{{ $asset["Asset"]->name }}</td>
							<td>{{ number_format($asset["AssetValue"]) }}</td>
							<td><span></span></td>
						</tr>
						<tr>
							<td><span></span></td>
							<td>{{ $asset["Depreciation"]->name }}</td>
							<td>{{ number_format($asset["DepreciationValue"]) }}</td>
							<td><span></span></td>
						</tr>
						<tr>
							<td><span></span></td>
							<td>Nilai buku {{ $asset["Asset"]->name }}</td>
							<td>{{ number_format($asset["BookValue"]) }}</td>
							<td><span></span></td>
						</tr>
					@endforeach
					<tr>
						<th colspan="2">Total Aktiva</th>
						<th colspan="2">{{ number_format($report["BalanceSheet"]["Totals"]["Assets"]) }}</th>
					</tr>
				</tbody>
				<thead class="thead-dark">
					<tr>
						<th colspan="4">PASIVA</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $report["BalanceSheet"]["Liabilities"] as $liability )
						<tr>
							<td><span></span></td>
							<td>{{ $liability["Account"]->name }}</td>
							<td><span></span></td>
							<td>{{ number_format((-1 * $liability["Value"])) }}</td>
						</tr>
					@endforeach
					<tr>
						<th colspan="2">Total Kewajiban</th>
						<th colspan="2">{{ number_format((-1 * ($report["BalanceSheet"]["Totals"]["Liabilities"]))) }}</th>
					</tr>

					@foreach( $report["BalanceSheet"]["Equity"] as $equity )
						<tr>
							<td><span></span></td>
							<td>{{ $equity["Account"]->name }}</td>
							<td><span></span></td>
							<td>{{ number_format((-1 * $equity["Value"])) }}</td>
						</tr>
					@endforeach
					<tr>
						<th colspan="2">Total Ekuitas</th>
						<th colspan="2">{{ number_format((-1 * ($report["BalanceSheet"]["Totals"]["Equity"]))) }}</th>
					</tr>

					<tr>
						<th colspan="2">Total Pasiva</th>
						<th colspan="2">{{ number_format((-1 * ($report["BalanceSheet"]["Totals"]["Liabilities"] + $report["BalanceSheet"]["Totals"]["Equity"]))) }}</th>
					</tr>
				</tbody>
			</table>
		</div>

	</div>

@endsection