@section('pageTitle')
    Worksheet
@endsection

@extends('master')

@section('content')
	@parent

	<div class="container">
		<h1>Worksheet untuk periode: {{ $period->start }} sampai {{ $period->end }} ( {{ $period->comment }} )</h1>

		<div class="w-50 mx-auto mt-5">
			<h2 class="text-center">TRIAL BALANCE</h2>
			<table class="table table-bordered">
				<thead class="thead-dark">
					<tr>
						<th>Account</th>
						<th>Debit</th>
						<th>Credit</th>
					</tr>
				</thead>
				<tbody>
					@php
						$totalDebit = 0;
						$totalCredit = 0;
					@endphp

					@foreach( $accounts as $account )
						<tr>
							<td>{{ $account->name }}</td>

							@php
								$accountSummary = $account->getAccumulationOfAllTransactionInPeriod($period, ["'NORMAL'", "'CORRECTION'"]);
							@endphp
							@if( ($account->normal_balance_position == "DEBIT" &&  $accountSummary >= 0) || ($account->normal_balance_position == "CREDIT" &&  $accountSummary < 0) )
								<td>{{ number_format(abs($accountSummary)) }}</td>
								<td><span></span></td>
								@php
									$totalDebit += abs($accountSummary);
								@endphp
							@else
								<td><span></span></td>
								<td>{{ number_format(abs($accountSummary)) }}</td>
								@php
									$totalCredit += abs($accountSummary);
								@endphp
							@endif
						</tr>
					@endforeach
					<tr>
						<th>TOTAL</th>
						<th>{{ number_format($totalDebit) }}</th>
						<th>{{ number_format($totalCredit) }}</th>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="w-50 mx-auto mt-5">
			<h2 class="text-center">ADJUSTMENT</h2>
			<table class="table table-bordered">
				<thead class="thead-dark">
					<tr>
						<th>Account</th>
						<th>Debit</th>
						<th>Credit</th>
					</tr>
				</thead>
				@php
					$totalDebit = 0;
					$totalCredit = 0;
				@endphp

				@foreach( $accounts as $account )
					<tr>
						<td>{{ $account->name }}</td>

						@php
							$accountSummary = $account->getAccumulationOfAllTransactionInPeriod($period, ["'ADJUSTMENT'"]) - $period->getOpeningBalance($account);
						@endphp
						@if( ($account->normal_balance_position == "DEBIT" &&  $accountSummary >= 0) || ($account->normal_balance_position == "CREDIT" &&  $accountSummary < 0) )
							<td>{{ number_format(abs($accountSummary)) }}</td>
							<td><span></span></td>
							@php
								$totalDebit += abs($accountSummary);
							@endphp
						@else
							<td><span></span></td>
							<td>{{ number_format(abs($accountSummary)) }}</td>
							@php
								$totalCredit += abs($accountSummary);
							@endphp
						@endif
					</tr>
				@endforeach
				<tr>
					<th>TOTAL</th>
					<th>{{ number_format($totalDebit) }}</th>
					<th>{{ number_format($totalCredit) }}</th>
				</tr>
			</table>
		</div>

		<div class="w-50 mx-auto mt-5">
			<h2>ADJUSTED TRIAL BALANCE</h2>
			<table class="table table-bordered">
				<thead class="thead-dark">
					<tr>
						<th>Account</th>
						<th>Debit</th>
						<th>Credit</th>
					</tr>
				</thead>
				@php
					$totalDebit = 0;
					$totalCredit = 0;
				@endphp

				@foreach( $accounts as $account )
					<tr>
						<td>{{ $account->name }}</td>

						@php
							$accountSummary = $account->getAccumulationOfAllTransactionInPeriod($period, ["'NORMAL'", "'CORRECTION'", "'ADJUSTMENT'"]);
						@endphp
						@if( ($account->normal_balance_position == "DEBIT" &&  $accountSummary >= 0) || ($account->normal_balance_position == "CREDIT" &&  $accountSummary < 0) )
							<td>{{ number_format(abs($accountSummary)) }}</td>
							<td><span></span></td>
							@php
								$totalDebit += abs($accountSummary);
							@endphp
						@else
							<td><span></span></td>
							<td>{{ number_format(abs($accountSummary)) }}</td>
							@php
								$totalCredit += abs($accountSummary);
							@endphp
						@endif
					</tr>
				@endforeach
				<tr>
					<th>TOTAL</th>
					<th>{{ number_format($totalDebit) }}</th>
					<th>{{ number_format($totalCredit) }}</th>
				</tr>
			</table>
		</div>

		<div class="w-50 mx-auto mt-5">
			<h2 class="text-center">POST CLOSING TRIAL BALANCE</h2>
			<table class="table table-bordered">
				<thead class="thead-dark">
					<tr>
						<th>Account</th>
						<th>Debit</th>
						<th>Credit</th>
					</tr>
				</thead>
				@php
					$totalDebit = 0;
					$totalCredit = 0;
				@endphp

				@foreach( $accounts as $account )
					<tr>
						<td>{{ $account->name }}</td>

						@php
							$accountSummary = $account->getAccumulationOfAllTransactionInPeriod($period, ["'NORMAL'", "'CORRECTION'", "'ADJUSTMENT'", "'CLOSING'"]);
						@endphp
						@if( ($account->normal_balance_position == "DEBIT" &&  $accountSummary >= 0) || ($account->normal_balance_position == "CREDIT" &&  $accountSummary < 0) )
							<td>{{ number_format(abs($accountSummary)) }}</td>
							<td><span></span></td>
							@php
								$totalDebit += abs($accountSummary);
							@endphp
						@else
							<td><span></span></td>
							<td>{{ number_format(abs($accountSummary)) }}</td>
							@php
								$totalCredit += abs($accountSummary);
							@endphp
						@endif
					</tr>
				@endforeach
				<tr>
					<th>TOTAL</th>
					<th>{{ number_format($totalDebit) }}</th>
					<th>{{ number_format($totalDebit) }}</th>
				</tr>
			</table>
		</div>

	</div>

@endsection