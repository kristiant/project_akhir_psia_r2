@section('pageTitle')
    Jurnal
@endsection

@extends('master')

@section('content')
	@parent

	<div class="container">

		<div>
			<h1>Jurnal untuk periode: {{ $period->start }} sampai {{ $period->end }} ( {{ $period->comment }} )</h1>
		</div>

		<div class="mt-5">
			<table class="table table-bordered">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Tanggal</th>
			      <th scope="col">Transaksi</th>
			      <th scope="col">No.Ref.</th>
			      <th scope="col">Account</th>
			      <th scope="col">Debit</th>
			      <th scope="col">Credit</th>
			    </tr>
			  </thead>
			  <tbody>
			    @foreach( $period->getTransactions() as $transaction )
					<tr>
						<td>{{ $transaction->registered_at }}</td>
						<td>{{ $transaction->notes }}</td>
						<td>{{ $transaction->reference_number }}</td>
						<td></td>
						<td><span></span></td>
						<td><span></span></td>
					</tr>
					@foreach( $transaction->accounts()->orderBy('transaction_details.taccount','DESC')->get() as $detail )
						<tr>
							<td><span></span></td>
							<td><span></span></td>
							<td><span></span></td>
							<td>{{ $detail->name }}</td>
							@if( $detail->pivot->taccount == "DEBIT" )
								<td>{{ number_format($detail->pivot->value) }}</td>
								<td><span></span></td>
							@else
								<td><span></span></td>
								<td>{{ number_format($detail->pivot->value) }}</td>
							@endif
						</tr>
					@endforeach
				@endforeach
			  </tbody>
			</table>
		</div>

	</div>

@endsection