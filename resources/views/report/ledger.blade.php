@section('pageTitle')
    Buku Besar
@endsection

@extends('master')

@section('content')
	@parent

	<div class="container">
		
		<div>
			<h1>Buku Besar (ledger) untuk periode: {{ $period->start }} sampai {{ $period->end }} ( {{ $period->comment }} )</h1>
		</div>

		@foreach( $accounts as $account )
			<div class="mt-5">
				<h2>{{ $account->name }}, Kode: {{ $account->id }}</h2>
			</div>
			<table class="table table-bordered">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Tanggal</th>
						<th scope="col">Transaksi</th>
						<th scope="col">Debit</th>
						<th scope="col">Credit</th>
						<th scope="col">Saldo Debit</th>
						<th scope="col">Saldo Credit</th>
					</tr>
				</thead>
				<tbody>
					@php
						$sisa_saldo = $period->getOpeningBalance($account);
					@endphp
					<tr>
						<td>-</td>
						<td>Saldo awal</td>
						<td><span></span></td>
						<td><span></span></td>
						@if( $account->normal_balance_position == "DEBIT" )
							<td>{{ number_format($sisa_saldo) }}</td>
							<td><span></span></td>
						@else
							<td><span></span></td>
							<td>{{ number_format($sisa_saldo) }}</td>
						@endif
					</tr>
					@foreach( $period->getTransactionDetails($account) as $detail )
						<tr>
							<td>{{ $detail->registered_at }}</td>
							<td>{{ $detail->notes }}</td>
							@if( $detail->taccount == "DEBIT" )
								<td>{{ number_format($detail->value) }}</td>
								<td><span></span></td>
							@else
								<td><span></span></td>
								<td>{{ number_format($detail->value) }}</td>
							@endif
							@php
								if( $account->normal_balance_position == $detail->taccount )
								{
								    $sisa_saldo += $detail->value;
								}
								else
								{
								    $sisa_saldo -= $detail->value;
								}
							@endphp
							@if( $account->normal_balance_position == "DEBIT" )
								<td>{{ number_format($sisa_saldo) }}</td>
								<td><span></span></td>
							@else
								<td><span></span></td>
								<td>{{ number_format($sisa_saldo) }}</td>
							@endif
						</tr>
					@endforeach
				</tbody>
			</table>
		@endforeach

	</div>

@endsection