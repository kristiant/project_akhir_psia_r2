@section('pageTitle')
    Kartu Stok
@endsection

@extends('master')

@section('content')

	<div class="container">

		<div>
			<h1>Kartu Stok untuk {{ $product->nama }}</h1>
		</div>

		<br>

		<table class="table table-bordered">
			<thead class="thead-dark text-center">
				<tr>
					<th scope="col" rowspan="2">Tanggal</th>
					<th scope="col" colspan="3">Pembelian</th>
					<th scope="col" colspan="3">Penjualan</th>
					<th scope="col" colspan="3">Saldo Sediaan</th>
				</tr>
				<tr>
				@for( $i = 0; $i < 3; $i++ )
					<th scope="col">Jumlah</th>
					<th scope="col">Harga/Unit</th>
					<th scope="col">Total Harga</th>
				@endfor
				</tr>
			</thead>
			<tbody>
				<tr>
				@for( $i = 0; $i < 7; $i++ )
					<td><span></span></td>
				@endfor
					<td>0</td>
					<td><span></span></td>
					<td><span></span></td>
				</tr>
				@foreach( $histories as $item )
				<tr>
					<td>{{ date("d M Y", strtotime($item->tanggal)) }}</td>
					@if($item->jumlah > 0)
						<td>{{ $item->jumlah }}</td>
						<td>{{ number_format($item->harga) }}</td>
						<td>{{ number_format($item->jumlah * $item->harga) }}</td>
					@else
						<td><span></span></td>
						<td><span></span></td>
						<td><span></span></td>
					@endif

					@if( $item->jumlah < 0 )
						<td>{{ abs($item->jumlah) }}</td>
						<td>{{ number_format($item->harga) }}</td>
						<td>{{ number_format(abs($item->jumlah * $item->harga)) }}</td>
					@else
						<td><span></span></td>
						<td><span></span></td>
						<td><span></span></td>
					@endif

					<td>{{ $item->saldo_jumlah }}</td>
					<td>{{ number_format($item->saldo_harga) }}</td>
					<td>{{ number_format($item->saldo_jumlah * $item->saldo_harga) }}</td>
				</tr>
				@endforeach

				<tr>
					<td><b>TOTAL</b></td>
					@php
						$totalQtyBeli = 0;
						$totalHrgBeli = 0;
						$totalQtyJual = 0;
						$totalHrgJual = 0;
						$totalQtySaldo = 0;
						$totalHrgSaldo = 0;

						foreach( $histories as $item )
						{
							if($item->jumlah > 0)
							{
								$totalQtyBeli += $item->jumlah;
								$totalHrgBeli += $item->jumlah * $item->harga;
							}
							else
							{
								$totalQtyJual += abs($item->jumlah);
								$totalHrgJual += abs($item->jumlah * $item->harga);
							}

							$totalQtySaldo += $item->saldo_jumlah;
							$totalHrgSaldo += $item->saldo_jumlah * $item->saldo_harga;
						}
					@endphp
					<td><b>{{ number_format($totalQtyBeli) }}</b></td>
					<td><span></span></td>
					<td><b>{{ number_format($totalHrgBeli) }}</b></td>
					<td><b>{{ number_format($totalQtyJual) }}</b></td>
					<td><span></span></td>
					<td><b>{{ number_format($totalHrgJual) }}</b></td>
					<td><b>{{ number_format($totalQtySaldo) }}</b></td>
					<td><span></span></td>
					<td><b>{{ number_format($totalHrgSaldo) }}</b></td>
				</tr>
			</tbody>
		</table>

	</div>

@endsection