@section('pageTitle')
    Tutup Periode
@endsection

@extends('master')

@section('content')
    @parent

    <div class="card w-50 mx-auto" style="margin-top: 10%;">
        <div class="card-header">
            <h5 class="card-title text-center">Pilih Periode</h5>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('ClosePeriod.post.close') }}">
            @csrf
                <div class="form-group">
                    <select class="form-control col-8 mx-auto" name="period_id">
                        <option selected>-- Pilih Periode --</option>
                        @foreach($periods as $item)
                        	@if($item->is_closed == 0)
                            	<option value="{{ $item->id }}">{{ $item->id }} {{ $item->comment }}</option>
                            @endif
                        @endforeach
                    </select>
                    <br>
                    <label class="col-8 mx-auto" style="text-align: center;">Tanggal Closing</label>
                    <input type="text" name="close_date" class="form-control col-8 mx-auto" placeholder="Tanggal Closing" value="{{ date('Y-m-d H:i:s') }}">

                </div>
                <div class="form-group text-center">
                    <button type="submit" class="form-control btn btn-primary col-3" value="submit">Close Period</button>
                </div>
        </form>
      </div>
    </div>
@endsection