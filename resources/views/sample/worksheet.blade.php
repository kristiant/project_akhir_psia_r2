<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<h1>Worksheet untuk periode: {{ $period->start }} sampai {{ $period->end }} ( {{ $period->comment }} )</h1>

<p>NOTE1: UNTUK LIHAT DI BROWSER, JIKA TIDAK TERLIHAT FULL BISA DI ZOOM-OUT (CTRL -)</p>
<P>NOTE2: UNTUK YANG MEN-DESIGN VIEW, HARAP PAKAI INHERITANCE, KEEP CODE DRY (DON'T REPEAT YOURSELF)</P>

<h2>TRIAL BALANCE</h2>
<table border="1">
	<thead>
		<tr>
			<td>Account</td>
			<td>Debit</td>
			<td>Credit</td>
		</tr>
	</thead>
	@php
		$totalDebit = 0;
		$totalCredit = 0;
	@endphp

	@foreach( $accounts as $account )
		<tr>
			<td>{{ $account->name }}</td>

			@php
				$accountSummary = $account->getAccumulationOfAllTransactionInPeriod($period, ["'NORMAL'", "'CORRECTION'"]);
			@endphp
			@if( ($account->normal_balance_position == "DEBIT" &&  $accountSummary >= 0) || ($account->normal_balance_position == "CREDIT" &&  $accountSummary < 0) )
				<td>{{ abs($accountSummary) }}</td>
				<td>.</td>
				@php
					$totalDebit += abs($accountSummary);
				@endphp
			@else
				<td>.</td>
				<td>{{ abs($accountSummary) }}</td>
				@php
					$totalCredit += abs($accountSummary);
				@endphp
			@endif
		</tr>
	@endforeach
	<tr>
		<td>TOTAL</td>
		<td>{{ $totalDebit }}</td>
		<td>{{ $totalCredit }}</td>
	</tr>
</table>













<h2>ADJUSTMENT</h2>
<table border="1">
	<thead>
		<tr>
			<td>Account</td>
			<td>Debit</td>
			<td>Credit</td>
		</tr>
	</thead>
	@php
		$totalDebit = 0;
		$totalCredit = 0;
	@endphp

	@foreach( $accounts as $account )
		<tr>
			<td>{{ $account->name }}</td>

			@php
				$accountSummary = $account->getAccumulationOfAllTransactionInPeriod($period, ["'ADJUSTMENT'"]) - $period->getOpeningBalance($account);
			@endphp
			@if( ($account->normal_balance_position == "DEBIT" &&  $accountSummary >= 0) || ($account->normal_balance_position == "CREDIT" &&  $accountSummary < 0) )
				<td>{{ abs($accountSummary) }}</td>
				<td>.</td>
				@php
					$totalDebit += abs($accountSummary);
				@endphp
			@else
				<td>.</td>
				<td>{{ abs($accountSummary) }}</td>
				@php
					$totalCredit += abs($accountSummary);
				@endphp
			@endif
		</tr>
	@endforeach
	<tr>
		<td>TOTAL</td>
		<td>{{ $totalDebit }}</td>
		<td>{{ $totalCredit }}</td>
	</tr>
</table>













<h2>ADJUSTED TRIAL BALANCE</h2>
<table border="1">
	<thead>
		<tr>
			<td>Account</td>
			<td>Debit</td>
			<td>Credit</td>
		</tr>
	</thead>
	@php
		$totalDebit = 0;
		$totalCredit = 0;
	@endphp

	@foreach( $accounts as $account )
		<tr>
			<td>{{ $account->name }}</td>

			@php
				$accountSummary = $account->getAccumulationOfAllTransactionInPeriod($period, ["'NORMAL'", "'CORRECTION'", "'ADJUSTMENT'"]);
			@endphp
			@if( ($account->normal_balance_position == "DEBIT" &&  $accountSummary >= 0) || ($account->normal_balance_position == "CREDIT" &&  $accountSummary < 0) )
				<td>{{ abs($accountSummary) }}</td>
				<td>.</td>
				@php
					$totalDebit += abs($accountSummary);
				@endphp
			@else
				<td>.</td>
				<td>{{ abs($accountSummary) }}</td>
				@php
					$totalCredit += abs($accountSummary);
				@endphp
			@endif
		</tr>
	@endforeach
	<tr>
		<td>TOTAL</td>
		<td>{{ $totalDebit }}</td>
		<td>{{ $totalCredit }}</td>
	</tr>
</table>













<h2>POST CLOSING TRIAL BALANCE</h2>
<table border="1">
	<thead>
		<tr>
			<td>Account</td>
			<td>Debit</td>
			<td>Credit</td>
		</tr>
	</thead>
	@php
		$totalDebit = 0;
		$totalCredit = 0;
	@endphp

	@foreach( $accounts as $account )
		<tr>
			<td>{{ $account->name }}</td>

			@php
				$accountSummary = $account->getAccumulationOfAllTransactionInPeriod($period, ["'NORMAL'", "'CORRECTION'", "'ADJUSTMENT'", "'CLOSING'"]);
			@endphp
			@if( ($account->normal_balance_position == "DEBIT" &&  $accountSummary >= 0) || ($account->normal_balance_position == "CREDIT" &&  $accountSummary < 0) )
				<td>{{ abs($accountSummary) }}</td>
				<td>.</td>
				@php
					$totalDebit += abs($accountSummary);
				@endphp
			@else
				<td>.</td>
				<td>{{ abs($accountSummary) }}</td>
				@php
					$totalCredit += abs($accountSummary);
				@endphp
			@endif
		</tr>
	@endforeach
	<tr>
		<td>TOTAL</td>
		<td>{{ $totalDebit }}</td>
		<td>{{ $totalCredit }}</td>
	</tr>
</table>

</body>
</html>