<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<h1>Jurnal untuk periode: {{ $period->start }} sampai {{ $period->end }} ( {{ $period->comment }} )</h1>

<p>NOTE1: UNTUK LIHAT DI BROWSER, JIKA TIDAK TERLIHAT FULL BISA DI ZOOM-OUT (CTRL -)</p>
<P>NOTE2: UNTUK YANG MEN-DESIGN VIEW, HARAP PAKAI INHERITANCE, KEEP CODE DRY (DON'T REPEAT YOURSELF)</P>

<table border="1">
	<thead>
		<tr>
			<td>Tanggal</td>
			<td>Transaksi</td>
			<td>NoRef</td>
			<td>Account</td>
			<td>Debit</td>
			<td>Credit</td>
		</tr>
	</thead>
	@foreach( $period->getTransactions() as $transaction )
		<tr>
			<td>{{ $transaction->registered_at }}</td>
			<td>{{ $transaction->notes }}</td>
			<td>{{ $transaction->reference_number }}</td>
			<td>.</td>
			<td>.</td>
			<td>.</td>
		</tr>
		@foreach( $transaction->accounts()->orderBy('transaction_details.taccount','DESC')->get() as $detail )
			<tr>
				<td>.</td>
				<td>.</td>
				<td>.</td>
				<td>{{ $detail->account->name }}</td>
				@if( $detail->pivot->taccount == "DEBIT" )
					<td>{{ $detail->pivot->value }}</td>
					<td>.</td>
				@else
					<td>.</td>
					<td>{{ $detail->pivot->value }}</td>
				@endif
			</tr>
		@endforeach
	@endforeach
</table>

</body>
</html>