<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<h1>Laporan keuangan (report) untuk periode: {{ $period->start }} sampai {{ $period->end }} ( {{ $period->comment }} )</h1>

<p>NOTE1: UNTUK LIHAT DI BROWSER, JIKA TIDAK TERLIHAT FULL BISA DI ZOOM-OUT (CTRL -)</p>
<P>NOTE2: UNTUK YANG MEN-DESIGN VIEW, HARAP PAKAI INHERITANCE, KEEP CODE DRY (DON'T REPEAT YOURSELF)</P>

<h2>LAPORAN LABA RUGI</h2>
<table border="1">
	<tr>
		<td colspan="2">INCOME</td>
	</tr>
	@foreach( $report["IncomeStatement"]["Incomes"] as $income )
		<tr>
			<td>{{ $income["Account"]->name }}</td>
			<td>{{ (-1 * $income["Value"]) }}</td>
		</tr>
	@endforeach
	<tr>
		<td>Total Income:</td>
		<td>{{ (-1 * $report["IncomeStatement"]["Totals"]["Incomes"]) }}</td>
	</tr>

	<tr>
		<td colspan="2">EXPENSE</td>
	</tr>
	@foreach( $report["IncomeStatement"]["Expenses"] as $expense )
		<tr>
			<td>{{ $expense["Account"]->name }}</td>
			<td>{{ $expense["Value"] }}</td>
		</tr>
	@endforeach
	<tr>
		<td>Total Expense:</td>
		<td>{{ $report["IncomeStatement"]["Totals"]["Expenses"] }}</td>
	</tr>

	<tr>
		<td colspan="2">KESIMPULAN</td>
	</tr>
	<tr>
		<td>LABA/RUGI:</td>
		<td>{{ (-1 * ($report["IncomeStatement"]["Totals"]["Incomes"] + $report["IncomeStatement"]["Totals"]["Expenses"]) ) }}</td>
	</tr>
</table>


<h2>LAPORAN PERUBAHAN EKUITAS</h2>
<p>Modal pada awal periode: {{ $report["EquityStatement"]["OpeningEquity"] }}</p>
<p>laba/rugi: {{ $report["EquityStatement"]["Income/Expense"] }}</p>
<p>prive: {{ $report["EquityStatement"]["Prive"] }}</p>
<p>
	Modal pada akhir periode: 
	{{ (
		$report["EquityStatement"]["OpeningEquity"]
		+ $report["EquityStatement"]["Income/Expense"]
		- $report["EquityStatement"]["Prive"]
	) }}
</p>


<h2>LAPORAN NERACA</h2>
<table border="1">
	<tr>
		<td>AKTIVA</td>
		<td>.</td>
		<td>.</td>
		<td>.</td>
	</tr>
	@foreach( $report["BalanceSheet"]["Assets"]["Normal"] as $asset )
		<tr>
			<td>.</td>
			<td>{{ $asset["Account"]->name }}</td>
			<td>{{ $asset["Value"] }}</td>
			<td>.</td>
		</tr>
	@endforeach
	@foreach( $report["BalanceSheet"]["Assets"]["Depreciating"] as $asset )
		<tr>
			<td>.</td>
			<td>.</td>
			<td>.</td>
			<td>.</td>
		</tr>
		<tr>
			<td>.</td>
			<td>{{ $asset["Asset"]->name }}</td>
			<td>{{ $asset["AssetValue"] }}</td>
			<td>.</td>
		</tr>
		<tr>
			<td>.</td>
			<td>{{ $asset["Depreciation"]->name }}</td>
			<td>{{ $asset["DepreciationValue"] }}</td>
			<td>.</td>
		</tr>
		<tr>
			<td>.</td>
			<td>Nilai buku {{ $asset["Asset"]->name }}</td>
			<td>{{ $asset["BookValue"] }}</td>
			<td>.</td>
		</tr>
	@endforeach
	<tr>
		<td colspan="2">Total Aktiva:</td>
		<td colspan="2">{{ $report["BalanceSheet"]["Totals"]["Assets"] }}</td>
	</tr>


	<tr>
		<td>.</td>
		<td>.</td>
		<td>.</td>
		<td>.</td>
	</tr>
	<tr>
		<td>.</td>
		<td>.</td>
		<td>.</td>
		<td>.</td>
	</tr>



	<tr>
		<td>PASIVA</td>
		<td>.</td>
		<td>.</td>
		<td>.</td>
	</tr>
	@foreach( $report["BalanceSheet"]["Liabilities"] as $liability )
		<tr>
			<td>.</td>
			<td>{{ $liability["Account"]->name }}</td>
			<td>.</td>
			<td>{{ (-1 * $liability["Value"]) }}</td>
		</tr>
	@endforeach
	<tr>
		<td colspan="2">Total Kewajiban:</td>
		<td colspan="2">{{ (-1 * ($report["BalanceSheet"]["Totals"]["Liabilities"])) }}</td>
	</tr>

	@foreach( $report["BalanceSheet"]["Equity"] as $equity )
		<tr>
			<td>.</td>
			<td>{{ $equity["Account"]->name }}</td>
			<td>.</td>
			<td>{{ (-1 * $equity["Value"]) }}</td>
		</tr>
	@endforeach
	<tr>
		<td colspan="2">Total Ekuitas:</td>
		<td colspan="2">{{ (-1 * ($report["BalanceSheet"]["Totals"]["Equity"])) }}</td>
	</tr>

	<tr>
		<td colspan="2">Total Pasiva:</td>
		<td colspan="2">{{ (-1 * ($report["BalanceSheet"]["Totals"]["Liabilities"] + $report["BalanceSheet"]["Totals"]["Equity"])) }}</td>
	</tr>

	
</table>

</body>
</html>