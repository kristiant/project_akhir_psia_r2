<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<h1>Buku Besar (ledger) untuk periode: {{ $period->start }} sampai {{ $period->end }} ( {{ $period->comment }} )</h1>

<p>NOTE1: UNTUK LIHAT DI BROWSER, JIKA TIDAK TERLIHAT FULL BISA DI ZOOM-OUT (CTRL -)</p>
<P>NOTE2: UNTUK YANG MEN-DESIGN VIEW, HARAP PAKAI INHERITANCE, KEEP CODE DRY (DON'T REPEAT YOURSELF)</P>

@foreach( $accounts as $account )
	<h2>{{ $account->name }}, Kode: {{ $account->id }}</h2>
	<table border="1">
		<thead>
			<tr>
				<td>Tanggal</td>
				<td>Transaksi</td>
				<td>Debit</td>
				<td>Credit</td>
				<td>Saldo Debit</td>
				<td>Saldo Credit</td>
			</tr>
		</thead>


		@php
			$sisa_saldo = $period->getOpeningBalance($account);
		@endphp
		<tr>
			<td>-</td>
			<td>Saldo awal</td>
			<td>.</td>
			<td>.</td>
			@if( $account->normal_balance_position == "DEBIT" )
				<td>{{ $sisa_saldo }}</td>
				<td>.</td>
			@else
				<td>.</td>
				<td>{{ $sisa_saldo }}</td>
			@endif
		</tr>
		@foreach( $period->getTransactionDetails($account) as $detail )
			<tr>
				<td>{{ $detail->registered_at }}</td>
				<td>{{ $detail->notes }}</td>
				@if( $detail->taccount == "DEBIT" )
					<td>{{ $detail->value }}</td>
					<td>.</td>
				@else
					<td>.</td>
					<td>{{ $detail->value }}</td>
				@endif
				@php
					if( $account->normal_balance_position == $detail->taccount )
					{
					    $sisa_saldo += $detail->value;
					}
					else
					{
					    $sisa_saldo -= $detail->value;
					}
				@endphp
				@if( $account->normal_balance_position == "DEBIT" )
					<td>{{ $sisa_saldo }}</td>
					<td>.</td>
				@else
					<td>.</td>
					<td>{{ $sisa_saldo }}</td>
				@endif
			</tr>
		@endforeach
	</table>
@endforeach





</body>
</html>